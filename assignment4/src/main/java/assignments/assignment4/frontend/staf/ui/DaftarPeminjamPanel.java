package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.RefreshLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class DaftarPeminjamPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, pilihLbl, daftarLbl;
    private MyComboBox<String> bukuBox;
    private MyButton lihatBtn, kembaliBtn;
    private MyPanel panelBukuBox, panelDaftar, panelLihatbtn, panelKembaliBtn;
    private MyScrollPane scrollPane;

    public DaftarPeminjamPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        daftarLbl.setText("");
        String[] daftarBuku = RefreshLogic.getDaftarBukuTerbaru();  // Ambil daftar buku terbaru dari SistakaNG
        bukuBox.setModel(new DefaultComboBoxModel<>(daftarBuku));   // Set model comboBox dengan daftar buku terbaru
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Lihat Daftar Peminjam");
        pilihLbl = new MyLabel("Pilih Buku");
        daftarLbl = new MyLabel();
        lihatBtn = new MyButton("Lihat");
        kembaliBtn = new MyButton("Kembali");
        bukuBox = new MyComboBox<>(new String[0]);
        panelBukuBox = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelLihatbtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelDaftar = new MyPanel(new GridBagLayout());
        scrollPane = new MyScrollPane(panelDaftar);
    }

    @Override
    public void addListener() {
        lihatBtn.addActionListener(e -> {
            if (bukuBox.getSelectedIndex() == -1) { // Jika tidak ada buku yang dipilih
                new MyDialogBox(main.getFrame(), "Silahkan memilih buku", "Warning", MyDialogBox.WARNING_MESSAGE);
            } else {
                String daftar = SistakaNG.daftarPeminjam(SistakaNG.getDaftarBuku().get(bukuBox.getSelectedIndex()));
                daftarLbl.setText(daftar);
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(20, 0, 5, 0));

        panelBukuBox.add(bukuBox);
        panelLihatbtn.add(lihatBtn);
        panelKembaliBtn.add(kembaliBtn);
        panelDaftar.add(daftarLbl);

        add(heading);
        add(pilihLbl);
        add(panelBukuBox);
        add(scrollPane);
        add(panelLihatbtn);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        pilihLbl.setSubHeading();
        daftarLbl.setBody();
        kembaliBtn.setSecondary();
        panelDaftar.setMaximumSize(new Dimension(400,600));
    }
}
