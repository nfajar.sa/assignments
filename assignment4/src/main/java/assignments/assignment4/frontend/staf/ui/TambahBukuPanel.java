package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.RefreshLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class TambahBukuPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, judulLbl, penulisLbl,
            penerbitLbl, kategoriLbl, stokLbl, space;
    private MyTextField judulField, penulisField, penerbitField, stokField;
    private MyComboBox<String> kategoriBox;
    private MyButton tambahBtn, kembaliBtn;
    private MyPanel panelTambahBtn, panelKembaliBtn, panelJudulField,
            panelPenulisField, panelPenerbitField, panelKategoriBox, panelStokField;

    public TambahBukuPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }


    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        String[] daftarkategori = RefreshLogic.getDaftarKategoriTerbaru();  // Mendapatkan daftar kategori terbaru
        kategoriBox.setModel(new DefaultComboBoxModel<>(daftarkategori));   // Mengganti model combo box dengan daftar kategori terbaru
        judulField.setText("");
        penulisField.setText("");
        penerbitField.setText("");
        stokField.setText("");
    }

    /**
     * Reset semua field dan combo box
     */
    private void reset(){
        judulField.setText("");
        penulisField.setText("");
        penerbitField.setText("");
        stokField.setText("");
        kategoriBox.setSelectedIndex(0);
    }

    @Override
    public void initComponent() {
        panelJudulField = new MyPanel(new FlowLayout());
        panelPenulisField = new MyPanel(new FlowLayout());
        panelPenerbitField = new MyPanel(new FlowLayout());
        panelStokField = new MyPanel(new FlowLayout());
        panelTambahBtn = new MyPanel(new FlowLayout());
        panelKembaliBtn = new MyPanel(new FlowLayout());
        heading = new MyLabel("Tambah Buku");
        judulLbl = new MyLabel("Judul");
        penulisLbl = new MyLabel("Penulis");
        penerbitLbl = new MyLabel("Penerbit");
        kategoriLbl = new MyLabel("Kategori");
        stokLbl = new MyLabel("Stok");
        space = new MyLabel();
        kategoriBox = new MyComboBox<>(new String[0]);
        panelKategoriBox = new MyPanel(new FlowLayout());
        judulField = new MyTextField();
        penulisField = new MyTextField();
        penerbitField = new MyTextField();
        stokField = new MyTextField();
        tambahBtn = new MyButton("Tambah");
        kembaliBtn = new MyButton("Kembali");
    }

    @Override
    public void addListener() {
        tambahBtn.addActionListener(e -> {
            if (isNumeric(stokField.getText().trim()) &&        // Jika stok buku yang dimasukkan adalah angka,
                    !judulField.getText().isBlank() &&          // judul buku yang dimasukkan tidak kosong,
                    !penulisField.getText().isBlank() &&        // penulis buku yang dimasukkan tidak kosong,
                    !penerbitField.getText().isBlank() &&       // penerbit buku yang dimasukkan tidak kosong,
                    kategoriBox.getSelectedItem() != null) {    // dan kategori buku yang dimasukkan tidak kosong
                Buku bukuBaru = SistakaNG.addBuku(judulField.getText(), penulisField.getText(), penerbitField.getText(), (String) kategoriBox.getSelectedItem(), Integer.parseInt(stokField.getText().trim()));

                if(Integer.parseInt(stokField.getText()) <= 0){ // Jika stok buku yang dimasukkan adalah 0 atau negatif
                    new MyDialogBox(main.getFrame(),
                            "Stok harus lebih dari 0!",
                            "Warning", MyDialogBox.WARNING_MESSAGE);
                    stokField.setText("");
                } else if (bukuBaru == null) {  // Jika buku yang dimasukkan sudah ada
                    Buku bukuLama = SistakaNG.findBuku(judulField.getText(), penulisField.getText());
                    if (bukuLama != null) { // Safe
                        new MyDialogBox(main.getFrame(),
                                "Buku " + bukuLama.getJudul() + " oleh " + bukuLama.getPenulis() + " sudah pernah ditambahkan",
                                "Warning", MyDialogBox.WARNING_MESSAGE);
                        reset();
                    }
                    else System.out.println("Entahlah");
                } else {    // Jika buku yang dimasukkan belum ada
                    new MyDialogBox(main.getFrame(),
                            "Buku " + bukuBaru.getJudul() + " oleh " + bukuBaru.getPenulis() + " berhasil ditambahkan!",
                            "Success!", MyDialogBox.INFORMATION_MESSAGE);
                    main.setPanel("staf");
                }
            } else {
                new MyDialogBox(main.getFrame(),
                        "Silahkan isi semua field yang tersedia!",
                        "Warning", MyDialogBox.WARNING_MESSAGE);
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(90, 0, 10, 0));
        space.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        penulisLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        penerbitLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        kategoriLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        stokLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        panelJudulField.add(judulField);
        panelPenulisField.add(penulisField);
        panelPenerbitField.add(penerbitField);
        panelStokField.add(stokField);
        panelTambahBtn.add(tambahBtn);
        panelKembaliBtn.add(kembaliBtn);
        panelKategoriBox.add(kategoriBox);

        add(heading);
        add(judulLbl);
        add(panelJudulField);
        add(penulisLbl);
        add(panelPenulisField);
        add(penerbitLbl);
        add(panelPenerbitField);
        add(kategoriLbl);
        add(panelKategoriBox);
        add(stokLbl);
        add(panelStokField);
        add(space);
        add(panelTambahBtn);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        judulLbl.setSubHeading();
        penulisLbl.setSubHeading();
        penerbitLbl.setSubHeading();
        stokLbl.setSubHeading();
        kategoriLbl.setSubHeading();
        kembaliBtn.setSecondary();
    }
}
