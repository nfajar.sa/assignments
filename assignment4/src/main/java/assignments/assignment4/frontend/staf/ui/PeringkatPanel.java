package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.style.MyButton;
import assignments.assignment4.frontend.style.MyLabel;
import assignments.assignment4.frontend.style.MyPanel;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class PeringkatPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, rankLbl, imageLbl;
    private MyButton kembaliBtn;
    private MyPanel panelKembaliBtn, panelRank;
    private ImageIcon image;

    public PeringkatPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        String rank = SistakaNG.handleRankingAnggota();
        if (rank.equals("Belum ada anggota yang terdaftar pada sistem")) {  // Jika tidak ada anggota yang terdaftar
            imageLbl.setIcon(image);    // Tampilkan gambar
            imageLbl.setBorder(BorderFactory.createEmptyBorder(80, 0, 10, 0));
            rankLbl.setVerticalAlignment(JLabel.TOP);
        } else {
            imageLbl.setIcon(null);     // Hapus gambar
            imageLbl.setBorder(null);
            rankLbl.setVerticalAlignment(JLabel.CENTER);
        }
        rankLbl.setText(rank);
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Peringkat");
        kembaliBtn = new MyButton("Kembali");
        rankLbl = new MyLabel();
        panelRank = new MyPanel(new BorderLayout());
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        image = new ImageIcon("assignment4/src/main/java/assignments/assignment4/frontend/assets/404.png");
        imageLbl = new MyLabel();
    }

    @Override
    public void addListener() {
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(90, 0, 10, 0));
        panelKembaliBtn.add(kembaliBtn);
        panelRank.add(imageLbl, BorderLayout.PAGE_START);
        panelRank.add(rankLbl, BorderLayout.CENTER);

        add(heading);
        add(panelRank);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        panelRank.setAlignmentY(Component.CENTER_ALIGNMENT);
        heading.setHeading();
        rankLbl.setBody();
        panelRank.setMaximumSize(new Dimension(400, 400));
        kembaliBtn.setSecondary();
        image.setImage(image.getImage().getScaledInstance(187, 181, Image.SCALE_DEFAULT));
        imageLbl.setHorizontalAlignment(JLabel.CENTER);
        rankLbl.setHorizontalAlignment(JLabel.CENTER);
    }
}
