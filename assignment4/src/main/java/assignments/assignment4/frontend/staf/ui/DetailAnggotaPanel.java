package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.RefreshLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class DetailAnggotaPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, pilihLbl, daftarLbl;
    private MyComboBox<String> anggotaBox;
    private MyButton lihatBtn, kembaliBtn;
    private MyPanel panelAnggotaBox, panelDaftar, panelLihatbtn, panelKembaliBtn;
    private MyScrollPane scrollPane;

    public DetailAnggotaPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        daftarLbl.setText("");
        String[] daftarAnggota = RefreshLogic.getDaftarAnggotaTerbaru();  // Ambil daftar anggota terbaru
        anggotaBox.setModel(new DefaultComboBoxModel<>(daftarAnggota));   // Set model comboBox dengan daftar anggota terbaru
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Lihat Detail Anggota");
        pilihLbl = new MyLabel("Pilih Anggota");
        daftarLbl = new MyLabel();
        lihatBtn = new MyButton("Lihat");
        kembaliBtn = new MyButton("Kembali");
        anggotaBox = new MyComboBox<>(new String[0]);
        panelAnggotaBox = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelLihatbtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelDaftar = new MyPanel(new GridBagLayout());
        scrollPane = new MyScrollPane(panelDaftar);
    }

    @Override
    public void addListener() {
        lihatBtn.addActionListener(e -> {
            if (anggotaBox.getSelectedIndex() == -1) {  // Jika tidak ada yang dipilih
                new MyDialogBox(main.getFrame(), "Silahkan memilih ID Anggota", "Warning", MyDialogBox.WARNING_MESSAGE);
            } else {
                String detail = SistakaNG.getDaftarAnggota().get(anggotaBox.getSelectedIndex()).detail();
                daftarLbl.setText(detail);
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(20, 0, 5, 0));

        panelAnggotaBox.add(anggotaBox);
        panelLihatbtn.add(lihatBtn);
        panelKembaliBtn.add(kembaliBtn);
        panelDaftar.add(daftarLbl);

        add(heading);
        add(pilihLbl);
        add(panelAnggotaBox);
        add(scrollPane);
        add(panelLihatbtn);
        add(panelKembaliBtn);

    }

    @Override
    public void setStyle() {
        heading.setHeading();
        pilihLbl.setSubHeading();
        daftarLbl.setBody();
        kembaliBtn.setSecondary();
        panelDaftar.setMaximumSize(new Dimension(400,600));
    }
}
