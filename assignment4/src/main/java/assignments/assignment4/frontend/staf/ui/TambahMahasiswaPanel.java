package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Mahasiswa;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.style.*;


import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class TambahMahasiswaPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, namaLbl, tanggalLahirLbl, prodiLbl, angkatanLbl, space;
    private MyTextField namaField, tanggalLahirField, angkatanField;
    private MyComboBox<String> prodiBox;
    private MyButton tambahBtn, kembaliBtn;
    private final String[] daftarProdi = new String[]{"SIK", "SSI", "MIK", "MTI", "DIK"};
    private MyPanel panelTambahBtn, panelKembaliBtn, panelNamaField,
            panelTglField, panelAngkatanField, panelProdiBox;

    public TambahMahasiswaPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        namaField.setText("");
        angkatanField.setText("");
        tanggalLahirField.setText("");
        prodiBox.setSelectedIndex(0);
    }

    @Override
    public void initComponent() {
        panelNamaField = new MyPanel(new FlowLayout());
        panelTglField = new MyPanel(new FlowLayout());
        panelProdiBox = new MyPanel(new FlowLayout());
        panelAngkatanField = new MyPanel(new FlowLayout());
        panelTambahBtn = new MyPanel(new FlowLayout());
        panelKembaliBtn = new MyPanel(new FlowLayout());

        heading = new MyLabel("Tambah Mahasiswa");
        namaLbl = new MyLabel("Nama");
        tanggalLahirLbl = new MyLabel("Tanggal Lahir (DD/MM/YYYY)");
        prodiLbl = new MyLabel("Program Studi");
        angkatanLbl = new MyLabel("Angkatan");
        space = new MyLabel();

        namaField = new MyTextField();
        tanggalLahirField = new MyTextField();
        angkatanField = new MyTextField();
        prodiBox = new MyComboBox<>(daftarProdi);

        tambahBtn = new MyButton("Tambah");
        kembaliBtn = new MyButton("Kembali");
    }

    @Override
    public void addListener() {
        tambahBtn.addActionListener(e -> {
            Mahasiswa mahasiswaBaru = SistakaNG.addMahasiswa(namaField.getText(), tanggalLahirField.getText(), (String) prodiBox.getSelectedItem(), angkatanField.getText());
            if (mahasiswaBaru == null) {    // Jika mahasiswa baru gagal ditambahkan
                new MyDialogBox(main.getFrame(),
                        "Tidak dapat menambahkan mahasiswa silahkan periksa kembali input anda!",
                        "Warning", MyDialogBox.WARNING_MESSAGE);
            } else {       // Jika mahasiswa baru berhasil ditambahkan
                new MyDialogBox(main.getFrame(),
                        "Berhasil menambahkan mahasiswa dengan Id " + mahasiswaBaru.getId(),
                        "Success!", MyDialogBox.INFORMATION_MESSAGE);
                main.setPanel("staf");
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(90, 0, 50, 0));
        space.setBorder(BorderFactory.createEmptyBorder(30, 0, 0, 0));
        tanggalLahirLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        prodiLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        angkatanLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        panelNamaField.add(namaField);
        panelTglField.add(tanggalLahirField);
        panelProdiBox.add(prodiBox);
        panelAngkatanField.add(angkatanField);
        panelTambahBtn.add(tambahBtn);
        panelKembaliBtn.add(kembaliBtn);

        add(heading);
        add(namaLbl);
        add(panelNamaField);
        add(tanggalLahirLbl);
        add(panelTglField);
        add(prodiLbl);
        add(panelProdiBox);
        add(angkatanLbl);
        add(panelAngkatanField);
        add(space);
        add(panelTambahBtn);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        namaLbl.setSubHeading();
        tanggalLahirLbl.setSubHeading();
        prodiLbl.setSubHeading();
        angkatanLbl.setSubHeading();
        kembaliBtn.setSecondary();
    }
}
