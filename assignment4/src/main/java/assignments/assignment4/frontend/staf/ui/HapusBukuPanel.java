package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.RefreshLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class HapusBukuPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, bukuLbl, space;
    private MyComboBox<String> bukuBox;
    private MyButton hapusBtn, kembaliBtn;
    private MyPanel panelBukuBox, panelHapusBtn, panelKembaliBtn;


    public HapusBukuPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        String[] daftarBuku = RefreshLogic.getDaftarBukuTerbaru();  // Ambil daftar buku terbaru dari SistakaNG
        bukuBox.setModel(new DefaultComboBoxModel<>(daftarBuku));   // Set model comboBox dengan daftar buku terbaru
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Hapus Buku");
        bukuLbl = new MyLabel("Buku");
        space = new MyLabel();
        hapusBtn = new MyButton("Hapus");
        kembaliBtn = new MyButton("Kembali");
        bukuBox = new MyComboBox<>(new String[0]);
        panelBukuBox = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelHapusBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
    }

    @Override
    public void addListener() {
        hapusBtn.addActionListener(e -> {
            if (bukuBox.getSelectedItem() == null) {    // Jika tidak ada buku yang dipilih
                new MyDialogBox(main.getFrame(), "Silahkan memilih buku", "Warning", MyDialogBox.WARNING_MESSAGE);
            } else {
                int getIndex = bukuBox.getSelectedIndex();
                Buku tempBuku = SistakaNG.getDaftarBuku().get(getIndex);
                String message = SistakaNG.deleteBuku(tempBuku);

                if (tempBuku.getStokAwal() != tempBuku.getStok()) { // Jika buku yang dihapus masih dipinjam
                    new MyDialogBox(main.getFrame(), message, "Warning", MyDialogBox.WARNING_MESSAGE);
                } else {
                    new MyDialogBox(main.getFrame(), message, "Success!", MyDialogBox.INFORMATION_MESSAGE);
                    main.setPanel("staf");
                }
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(90, 0, 150, 0));
        space.setBorder(BorderFactory.createEmptyBorder(150, 0, 0, 0));

        panelHapusBtn.add(hapusBtn);
        panelKembaliBtn.add(kembaliBtn);
        panelBukuBox.add(bukuBox);

        add(heading);
        add(bukuLbl);
        add(panelBukuBox);
        add(space);
        add(panelHapusBtn);
        add(panelKembaliBtn);

    }

    @Override
    public void setStyle() {
        heading.setHeading();
        bukuLbl.setSubHeading();
        hapusBtn.setDanger();
        kembaliBtn.setSecondary();
    }
}
