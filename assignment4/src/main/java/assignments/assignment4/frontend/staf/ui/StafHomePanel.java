package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.style.*;

import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class StafHomePanel extends SistakaPanel implements PanelLogic {
    private MyButton tambahMahasiswa, tambahDosen, tambahKategori,
                    tambahBuku, hapusBuku, peringkatPertama,
                    detailAnggota, daftarPeminjamBuku, logout;
    private MyLabel heading;

    public StafHomePanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        // Get user name
        if (main.getUser() != null) heading.setText("Selamat datang kembali " + main.getUser().getNama() + "!");
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Selamat datang kembali!");
        tambahMahasiswa = new MyButton("Tambah Mahasiswa");
        tambahDosen = new MyButton("Tambah Dosen");
        tambahKategori = new MyButton("Tambah Kategori");
        tambahBuku = new MyButton("Tambah Buku");
        hapusBuku = new MyButton("Hapus Buku");
        peringkatPertama = new MyButton("3 Peringkat Pertama");
        detailAnggota = new MyButton("Detail Anggota");
        daftarPeminjamBuku = new MyButton("Daftar Peminjam Buku");
        logout = new MyButton("Logout");
    }

    @Override
    public void addListener() {
        tambahMahasiswa.addActionListener(e -> main.setPanel("tambahMhs"));
        tambahDosen.addActionListener(e -> main.setPanel("tambahDosen"));
        tambahKategori.addActionListener(e -> main.setPanel("tambahKategori"));
        tambahBuku.addActionListener(e -> main.setPanel("tambahBuku"));
        hapusBuku.addActionListener(e -> main.setPanel("hapusBuku"));
        peringkatPertama.addActionListener(e -> main.setPanel("peringkat"));
        detailAnggota.addActionListener(e -> main.setPanel("detailAnggota"));
        daftarPeminjamBuku.addActionListener(e -> main.setPanel("daftarPeminjam"));
        logout.addActionListener(e -> {
            main.setUser(null);
            main.setPanel("welcome");
        });
    }

    @Override
    public void mySetLayout() {
        setLayout(new GridLayout(10,1, 0, 20));
        add(heading);
        add(tambahMahasiswa);
        add(tambahDosen);
        add(tambahKategori);
        add(tambahBuku);
        add(hapusBuku);
        add(peringkatPertama);
        add(detailAnggota);
        add(daftarPeminjamBuku);
        add(logout);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        logout.setDanger();
    }
}
