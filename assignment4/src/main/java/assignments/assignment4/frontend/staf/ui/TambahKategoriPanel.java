package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Kategori;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class TambahKategoriPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, namaLbl, poinLbl, space;
    private MyTextField namaField, poinField;
    private MyButton tambahBtn, kembaliBtn;
    private MyPanel panelTambahBtn, panelKembaliBtn, panelNamaField, panelPoinField;

    public TambahKategoriPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        namaField.setText("");
        poinField.setText("");
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Tambah Kategori");
        namaLbl = new MyLabel("Nama");
        poinLbl = new MyLabel("Poin");
        space = new MyLabel();
        namaField = new MyTextField();
        poinField = new MyTextField();
        tambahBtn = new MyButton("Tambah");
        kembaliBtn = new MyButton("Kembali");
        panelNamaField = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelPoinField = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelTambahBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
    }

    @Override
    public void addListener() {
        tambahBtn.addActionListener(e -> {
            // Jika nama tidak kosong dan poin berupa angka (integer)
            if (!namaField.getText().isBlank() && isNumeric(poinField.getText())) {
                Kategori kategoriBaru = SistakaNG.addKategori(namaField.getText(), Integer.parseInt(poinField.getText().trim()));

                if (kategoriBaru != null) { // Jika kategori baru berhasil ditambahkan
                    new MyDialogBox(main.getFrame(),
                            "Kategori " + kategoriBaru.getNama() + " dengan poin " + kategoriBaru.getPoin() +" berhasil ditambahkan",
                            "Success!", MyDialogBox.INFORMATION_MESSAGE);
                    main.setPanel("staf");
                } else {    // Jika kategori baru gagal ditambahkan
                    Kategori kategori = SistakaNG.findKategori(namaField.getText());
                    if (kategori != null) new MyDialogBox(main.getFrame(),
                            "Kategori " + kategori.getNama() + " sudah pernah ditambahkan", "Warning", MyDialogBox.WARNING_MESSAGE);
                    refresh();
                }

            } else {    // Jika nama tidak kosong dan poin tidak berupa angka (integer)
                new MyDialogBox(main.getFrame(),
                        "Silahkan isi semua field yang  dengan sesuai!", "Warning", MyDialogBox.WARNING_MESSAGE);
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(100, 0, 105, 0));
        space.setBorder(BorderFactory.createEmptyBorder(105, 0, 0, 0));
        poinLbl.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        panelNamaField.add(namaField);
        panelPoinField.add(poinField);
        panelTambahBtn.add(tambahBtn);
        panelKembaliBtn.add(kembaliBtn);

        add(heading);
        add(namaLbl);
        add(panelNamaField);
        add(poinLbl);
        add(panelPoinField);
        add(space);
        add(panelTambahBtn);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        namaLbl.setSubHeading();
        poinLbl.setSubHeading();
        kembaliBtn.setSecondary();
    }
}