package assignments.assignment4.frontend.staf.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Dosen;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class TambahDosenPanel extends SistakaPanel implements PanelLogic {
    private MyButton tambahBtn, kembaliBtn;
    private MyPanel panelTambahBtn, panelKembaliBtn, panelNamaField;
    private MyLabel heading, namaLbl, space;
    private MyTextField namaField;
    public TambahDosenPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        namaField.setText("");
    }

    @Override
    public void initComponent() {
        tambahBtn = new MyButton("Tambah");
        kembaliBtn = new MyButton("Kembali");
        heading = new MyLabel("Tambah Dosen");
        namaLbl = new MyLabel("Nama");
        space = new MyLabel();
        namaField = new MyTextField();
        panelNamaField = new MyPanel(new FlowLayout());
        panelTambahBtn = new MyPanel(new FlowLayout());
        panelKembaliBtn = new MyPanel(new FlowLayout());
    }

    @Override
    public void addListener() {
        tambahBtn.addActionListener(e -> {
            if (namaField.getText().isBlank()) {    // Jika nama kosong
                new MyDialogBox(main.getFrame(),
                        "Tidak dapat menambahkan dosen silahkan periksa kembali input anda!",
                        "Warning", MyDialogBox.WARNING_MESSAGE);
            } else{   // Jika nama tidak kosong
                Dosen dosenBaru = SistakaNG.addDosen(namaField.getText());
                new MyDialogBox(main.getFrame(),
                        "Berhasil menambahkan dosen dengan id " + dosenBaru.getId(),
                        "Success!", MyDialogBox.INFORMATION_MESSAGE);
                main.setPanel("staf");
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("staf"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(100, 0, 140, 0));
        space.setBorder(BorderFactory.createEmptyBorder(140, 0, 0, 0));

        panelNamaField.add(namaField);
        panelTambahBtn.add(tambahBtn);
        panelKembaliBtn.add(kembaliBtn);
        add(heading);
        add(namaLbl);
        add(panelNamaField);
        add(space);
        add(panelTambahBtn);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        namaLbl.setSubHeading();
        kembaliBtn.setSecondary();
    }
}