package assignments.assignment4.frontend.style;

import javax.swing.*;
import java.awt.*;

/**
 * Implementasi JButton dengan mengatur style default dari button.
 */
public class MyButton extends JButton {
    private static final Dimension size = new Dimension(300, 30);

    public MyButton(String text){
        super(text);
        setBackground(MyColor.primary);
        setForeground(Color.WHITE);
        setFont(MyFont.subHeading);
        setFocusable(false);
        setPreferredSize(size);
    }

    public void setSecondary(){
        setBackground(Color.WHITE);
        setForeground(MyColor.primary);
        setBorder(BorderFactory.createLineBorder(MyColor.primary, 2));
    }

    public void setDanger(){
        Color red = MyColor.danger;
        setBackground(Color.decode("#FDEEEE"));
        setForeground(MyColor.danger);
        setBorder(BorderFactory.createLineBorder(MyColor.danger, 2));
    }
}
