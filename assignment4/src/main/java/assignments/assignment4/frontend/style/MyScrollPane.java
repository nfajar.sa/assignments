package assignments.assignment4.frontend.style;

import javax.swing.*;
import java.awt.*;

/**
 * Implementasi JScrollPane dengan mangatur ukuran dan scrollbar default panel.
 */
public class MyScrollPane extends JScrollPane {
    private static final Dimension size = new Dimension(400,600);
    public MyScrollPane(Component view) {
        super(view, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setMaximumSize(size);
        setPreferredSize(size);
    }

}
