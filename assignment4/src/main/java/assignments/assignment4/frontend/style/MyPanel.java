package assignments.assignment4.frontend.style;

import javax.swing.*;
import java.awt.*;

/**
 * Implementasi JPanel dengan mangatur ukuran dan background default panel.
 */
public class MyPanel extends JPanel {
    public MyPanel(LayoutManager layout) {
        super(layout);
        setBackground(Color.WHITE);
        setMaximumSize(new Dimension(300, 50));
    }
}
