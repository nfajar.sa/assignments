package assignments.assignment4.frontend.style;

import javax.swing.*;
import java.awt.*;

/**
 * Implementasi JTextField dengan mengatur style default textfield.
 */
public class MyTextField extends JTextField {
    private static final Dimension size = new Dimension(300, 30);
    public MyTextField(){
        setBorder(BorderFactory.createLineBorder(MyColor.dark, 1));
        setFont(MyFont.body);
        setPreferredSize(size);
    }
}
