package assignments.assignment4.frontend.style;

import javax.swing.*;

/**
 * Implementasi JOptionPane untuk menampilkan dialog box.
 */
public class MyDialogBox extends JOptionPane {
    public MyDialogBox(JFrame frame, String message, String title, int type) {
        JLabel messageLbl = new JLabel(message);
        messageLbl.setFont(MyFont.body);
        showMessageDialog(frame, messageLbl, title, type);
    }
}
