package assignments.assignment4.frontend.style;

import javax.swing.*;
import java.awt.*;

/**
 * Implementasi JLabel dengan mengatur style dari label.
 */
public class MyLabel extends JLabel {
    public MyLabel() {
    }

    public MyLabel(String text) {
        super(text);
        setForeground(MyColor.dark);
        setAlignmentX(Component.CENTER_ALIGNMENT);
        setHorizontalAlignment(JLabel.CENTER);
    }
    public void setHeading(){
        setFont(MyFont.heading1);
    }
    public void setSubHeading(){
        setFont(MyFont.subHeading);
    }
    public void setBody(){
        setFont(MyFont.body);
    }
}
