package assignments.assignment4.frontend.style;

import javax.swing.*;
import java.awt.*;

/**
 * Implementasi JComboBox yang dapat mengatur style default dari ComboBox.
 */
public class MyComboBox<T> extends JComboBox<T> {
    private static final Dimension size = new Dimension(300, 30);

    public MyComboBox(T[] items) {
        super(items);
        setFont(MyFont.body);
        setPreferredSize(size);
        setForeground(MyColor.dark);
    }
}
