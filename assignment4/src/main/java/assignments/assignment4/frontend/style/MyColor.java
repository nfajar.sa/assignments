package assignments.assignment4.frontend.style;

import java.awt.*;

/**
 * Customize Color
 */
public class MyColor {
    public static Color primary = Color.decode("#5038BC");
    public static Color dark = Color.decode("#333333");
    public static Color danger = Color.decode("#EB5757");
}
