package assignments.assignment4.frontend.style;

import java.awt.*;

/**
 * Customize Font.
 */
public class MyFont {
    public static Font heading1 = new Font("Poppins", Font.BOLD, 18);
    public static Font subHeading = new Font("Poppins", Font.BOLD, 12);
    public static Font body = new Font("Poppins", Font.PLAIN, 12);
}
