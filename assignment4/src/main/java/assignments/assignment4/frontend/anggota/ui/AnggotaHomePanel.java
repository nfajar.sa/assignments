package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.style.MyButton;
import assignments.assignment4.frontend.style.MyLabel;
import assignments.assignment4.frontend.style.MyPanel;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class AnggotaHomePanel extends SistakaPanel implements PanelLogic {
    private MyButton peminjaman, pengembalian, pembayaranDenda,
            detaiAnggota, logout;
    private MyLabel heading, space1, space2;
    private MyPanel panel;
    public AnggotaHomePanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        // Get user name
        if (main.getUser() != null) heading.setText("Selamat datang kembali " + main.getUser().getNama() + "!");
    }

    @Override
    public void initComponent() {
        space1 = new MyLabel();
        space2 = new MyLabel();
        heading = new MyLabel("Selamat datang kembali!");
        peminjaman = new MyButton("Peminjaman");
        pengembalian = new MyButton("Pengembalian");
        pembayaranDenda = new MyButton("Pembayaran Denda");
        detaiAnggota = new MyButton("Detail Anggota");
        logout = new MyButton("Logout");
        panel = new MyPanel(new GridLayout(6,1, 0, 20));
    }

    @Override
    public void addListener() {
        peminjaman.addActionListener(e -> main.setPanel("peminjaman"));
        pengembalian.addActionListener(e -> main.setPanel("pengembalian"));
        pembayaranDenda.addActionListener(e -> main.setPanel("pembayaran"));
        detaiAnggota.addActionListener(e -> main.setPanel("detailUser"));
        logout.addActionListener(e -> {
            main.setUser(null);
            main.setPanel("welcome");
        });
    }

    @Override
    public void mySetLayout() {
        setLayout(new BorderLayout());
        space1.setBorder(BorderFactory.createEmptyBorder(100, 0, 0, 0));
        space2.setBorder(BorderFactory.createEmptyBorder(100, 0, 0, 0));
        panel.add(heading);
        panel.add(peminjaman);
        panel.add(pengembalian);
        panel.add(pembayaranDenda);
        panel.add(detaiAnggota);
        panel.add(logout);

        add(space1, BorderLayout.PAGE_START);
        add(panel, BorderLayout.CENTER);
        add(space2, BorderLayout.PAGE_END);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        logout.setDanger();
    }
}
