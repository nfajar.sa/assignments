package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.RefreshLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

public class PeminjamanPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, bukuLbl, tanggalLbl, space;
    private MyComboBox<String> bukuBox;
    private MyTextField tanggalField;
    private MyButton pinjamBtn, kembaliBtn;
    private MyPanel panelBukuBox, panelTanggalField, panelPinjamBtn, panelKembaliBtn;

    public PeminjamanPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        tanggalField.setText("");
        String[] daftarBuku = RefreshLogic.getDaftarBukuTerbaru();  // Ambil daftar buku terbaru
        bukuBox.setModel(new DefaultComboBoxModel<>(daftarBuku));   // Set model combo box dengan daftar buku terbaru
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Pinjam Buku");
        bukuLbl = new MyLabel("Buku");
        tanggalLbl = new MyLabel("Tanggal Peminjaman (DD/MM/YYYY)");
        space = new MyLabel();
        bukuBox = new MyComboBox<>(new String[0]);
        tanggalField = new MyTextField();
        panelBukuBox = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelTanggalField = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelPinjamBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        pinjamBtn = new MyButton("Pinjam");
        kembaliBtn = new MyButton("Kembali");
    }

    @Override
    public void addListener() {
        pinjamBtn.addActionListener(e -> {
            if (bukuBox.getSelectedItem() == null || tanggalField.getText().isBlank()) {    // Jika buku atau tanggal kosong
                new MyDialogBox(main.getFrame(),
                        "Input tidak boleh kosong!",
                        "Warning", MyDialogBox.WARNING_MESSAGE);
            } else if (!isDateValid(tanggalField.getText())) {  // Jika tanggal tidak valid
                new MyDialogBox(main.getFrame(),
                        "Tanggal yang dimasukkan harus dalam format DD/MM/YYYY",
                        "Warning", MyDialogBox.WARNING_MESSAGE);
            } else{
                Buku tempBuku = SistakaNG.getDaftarBuku().get(bukuBox.getSelectedIndex());
                String message = SistakaNG.pinjamBuku(tempBuku, tanggalField.getText());
                new MyDialogBox(main.getFrame(), message, "Success", MyDialogBox.INFORMATION_MESSAGE);
                main.setPanel("anggota");
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("anggota"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(90, 0, 105, 0));
        tanggalLbl.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
        space.setBorder(BorderFactory.createEmptyBorder(0,0,105,0));
        panelBukuBox.add(bukuBox);
        panelTanggalField.add(tanggalField);
        panelPinjamBtn.add(pinjamBtn);
        panelKembaliBtn.add(kembaliBtn);

        add(heading);
        add(bukuLbl);
        add(panelBukuBox);
        add(tanggalLbl);
        add(panelTanggalField);
        add(space);
        add(panelPinjamBtn);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        bukuLbl.setSubHeading();
        tanggalLbl.setSubHeading();
        kembaliBtn.setSecondary();
    }
}
