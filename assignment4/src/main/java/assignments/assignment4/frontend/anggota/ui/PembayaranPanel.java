package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

// TODO: Implementasikan hal-hal yang diperlukan
public class PembayaranPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, jumlahLbl, space;
    private MyTextField jumlahField;
    private MyButton bayarBtn, kembaliBtn;
    private MyPanel panelJumlahField, panelBayarBtn, panelKembaliBtn;

    public PembayaranPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        jumlahField.setText("");
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Bayar Denda");
        jumlahLbl = new MyLabel("Jumlah Denda");
        space = new MyLabel();
        jumlahField = new MyTextField();
        bayarBtn = new MyButton("Bayar");
        kembaliBtn = new MyButton("Kembali");
        panelJumlahField = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelBayarBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
    }

    @Override
    public void addListener() {
        bayarBtn.addActionListener(e -> {
            if (jumlahField.getText().isBlank()) {  // Jika field jumlah denda kosong
                new MyDialogBox(main.getFrame(), 
                        "Silahkan isi nominal pembayaran!",
                        "Warning", MyDialogBox.WARNING_MESSAGE);
            } else if (!isNumeric(jumlahField.getText())) { // Jika field jumlah denda bukan angka
                new MyDialogBox(main.getFrame(),
                        "Jumlah bayar harus berupa angka!",
                        "Warning", MyDialogBox.WARNING_MESSAGE);
            } else {
                String message = SistakaNG.bayarDenda(Long.parseLong(jumlahField.getText()));
                new MyDialogBox(main.getFrame(),
                        message,
                        "Info", MyDialogBox.INFORMATION_MESSAGE);
                main.setPanel("anggota");
            }
        });
        kembaliBtn.addActionListener(e -> main.setPanel("anggota"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(90, 0, 150, 0));
        space.setBorder(BorderFactory.createEmptyBorder(150, 0, 0, 0));
        panelJumlahField.add(jumlahField);
        panelBayarBtn.add(bayarBtn);
        panelKembaliBtn.add(kembaliBtn);

        add(heading);
        add(jumlahLbl);
        add(panelJumlahField);
        add(space);
        add(panelBayarBtn);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        jumlahLbl.setSubHeading();
        kembaliBtn.setSecondary();
    }
}
