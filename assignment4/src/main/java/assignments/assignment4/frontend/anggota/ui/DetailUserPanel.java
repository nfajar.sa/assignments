package assignments.assignment4.frontend.anggota.ui;

import assignments.assignment4.backend.pengguna.Anggota;
import assignments.assignment4.frontend.HomeGUI;
import assignments.assignment4.frontend.SistakaPanel;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class DetailUserPanel extends SistakaPanel implements PanelLogic {
    private MyLabel heading, daftarLbl;
    private MyButton kembaliBtn;
    private MyPanel panelDaftar, panelKembaliBtn;
    private MyScrollPane scrollPane;

    public DetailUserPanel(HomeGUI main) {
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void refresh() {
        // TODO: Implementasikan hal-hal yang diperlukan
        // Jika tidak diperlukan, Anda dapat mengabaikannya (kosongkan method ini)
        daftarLbl.setText(((Anggota) main.getUser()).detail()); // set label dengan detail anggota
    }

    @Override
    public void initComponent() {
        heading = new MyLabel("Lihat Detail Anggota");
        daftarLbl = new MyLabel();
        kembaliBtn = new MyButton("Kembali");
        panelKembaliBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelDaftar = new MyPanel(new GridBagLayout());
        scrollPane = new MyScrollPane(panelDaftar);
    }

    @Override
    public void addListener() {
        kembaliBtn.addActionListener(e -> main.setPanel("anggota"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(20, 0, 5, 0));

        panelKembaliBtn.add(kembaliBtn);
        panelDaftar.add(daftarLbl);

        add(heading);
        add(scrollPane);
        add(panelKembaliBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        daftarLbl.setBody();
        kembaliBtn.setSecondary();
        panelDaftar.setMaximumSize(new Dimension(400,600));
    }
}
