package assignments.assignment4.frontend;

import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.style.*;
import javax.swing.*;
import java.awt.*;

public class WelcomePanel extends SistakaPanel implements PanelLogic {
    private MyButton loginBtn, exitBtn;
    private MyLabel heading;
    private MyPanel panelLoginBtn, panelExitBtn;

    public WelcomePanel(HomeGUI homeGUI) {
        super(homeGUI);
        // TODO: Implementasikan hal-hal yang diperlukan
        // Welcome
        this.initComponent();
        this.setStyle();
        this.mySetLayout();
        this.addListener();
    }

    @Override
    public void initComponent() {
        panelExitBtn = new MyPanel(new FlowLayout());
        panelLoginBtn = new MyPanel(new FlowLayout());
        heading = new MyLabel("Welcome to SistakaNG");
        loginBtn = new MyButton("Login");
        exitBtn = new MyButton("Exit");
    }

    @Override
    public void addListener() {
        loginBtn.addActionListener(e -> main.setPanel("login"));
        exitBtn.addActionListener(e -> main.exit());
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        heading.setBorder(BorderFactory.createEmptyBorder(120, 0, 320, 0));
        panelExitBtn.add(exitBtn);
        panelLoginBtn.add(loginBtn);
        add(heading);
        add(panelLoginBtn);
        add(panelExitBtn);
    }

    @Override
    public void setStyle() {
        heading.setHeading();
        exitBtn.setDanger();
    }

    @Override
    public void refresh() {
        // ignored
    }
}