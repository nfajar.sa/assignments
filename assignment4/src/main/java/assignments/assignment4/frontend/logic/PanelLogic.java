package assignments.assignment4.frontend.logic;

/**
 * This interface is the logic for the panel.
 *
 * @author Fajar
 *
 */
public interface PanelLogic {
    /**
     * This method is used to initialize the components.
     */
    void initComponent();

    /**
     * This method is used to add the listener.
     */
    void addListener();

    /**
     * This method is used to set the layout.
     */
    void mySetLayout();

    /**
     * This method is used to set the style.
     */
    void setStyle();
}