package assignments.assignment4.frontend.logic;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.buku.Buku;
import assignments.assignment4.backend.buku.Kategori;
import assignments.assignment4.backend.pengguna.Anggota;

import java.util.ArrayList;

/**
 * This class is the logic for the refresh method.
 *
 * @author Fajar
 *
 */
public class RefreshLogic {

    /**
     * This method is used to get the latest book list.
     */
    public static String[] getDaftarBukuTerbaru() {
        ArrayList<Buku> tempDaftarBuku = SistakaNG.getDaftarBuku();
        if (tempDaftarBuku == null) {
            return new String[0];
        } else {
            ArrayList<String> tempDaftarBukuString = new ArrayList<>();
            for (Buku b:
                    tempDaftarBuku) {
                tempDaftarBukuString.add(b.toString());
            }
            return tempDaftarBukuString.toArray(new String[0]);
        }
    }

    /**
     * This method is used to get the latest category list.
     */
    public static String[] getDaftarKategoriTerbaru(){
        ArrayList<Kategori> tempDaftarKategori = SistakaNG.getDaftarKategori();
        if (tempDaftarKategori != null) {
            ArrayList<String> tempDaftarKategoriString = new ArrayList<>();
            for (Kategori c:
                    tempDaftarKategori) {
                tempDaftarKategoriString.add(c.getNama());
            }
            return tempDaftarKategoriString.toArray(new String[0]);
        } else {
            return new String[0];
        }
    }

    /**
     * This method is used to get the latest member list.
     */
    public static String[] getDaftarAnggotaTerbaru(){
        ArrayList<Anggota> tempDaftarAnggota = SistakaNG.getDaftarAnggota();
        if (tempDaftarAnggota == null) {
            return new String[0];
        } else {
            ArrayList<String> tempDaftarAnggotaString = new ArrayList<>();
            for (Anggota a:
                    tempDaftarAnggota) {
                tempDaftarAnggotaString.add(a.getId());
            }
            return tempDaftarAnggotaString.toArray(new String[0]);
        }
    }
}