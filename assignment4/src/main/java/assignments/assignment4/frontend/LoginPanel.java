package assignments.assignment4.frontend;

import assignments.assignment4.backend.SistakaNG;
import assignments.assignment4.backend.pengguna.Anggota;
import assignments.assignment4.backend.pengguna.Pengguna;
import assignments.assignment4.frontend.logic.PanelLogic;
import assignments.assignment4.frontend.style.*;

import javax.swing.*;
import java.awt.*;

// TODO: Implementasikan hal-hal yang diperlukan
public class LoginPanel extends SistakaPanel implements PanelLogic {
    private MyLabel headingTxt, idLbl, space;
    private MyTextField idField;
    private MyButton loginBtn, kembaliBtn;
    private MyPanel panelLoginBtn, panelField, panelBackBtn;

    public LoginPanel(HomeGUI main){
        super(main);
        // TODO: Implementasikan hal-hal yang diperlukan
        initComponent();
        addListener();
        mySetLayout();
        setStyle();
    }

    @Override
    public void initComponent() {
        panelField = new MyPanel(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelLoginBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        panelBackBtn = new MyPanel(new FlowLayout(FlowLayout.CENTER));
        headingTxt = new MyLabel("Login");
        idLbl =  new MyLabel("Masukkan ID anda untuk login ke SistakaNG");
        space = new MyLabel();
        idField = new MyTextField();
        loginBtn = new MyButton("Login");
        kembaliBtn = new MyButton("Kembali");
    }

    @Override
    public void addListener() {
        loginBtn.addActionListener(e -> {
            String text = idField.getText();
            if (text.isBlank()) {       // jika field kosong
                new MyDialogBox(main.getFrame(),
                        "Harap masukkan ID anda pada kotak di atas!",
                        "Warning",
                        JOptionPane.WARNING_MESSAGE);
            } else {
                Pengguna user = SistakaNG.handleLogin(text);
                if (user == null) {     // jika user tidak ditemukan
                    new MyDialogBox(main.getFrame(),
                            String.format("Pengguna dengan ID %s tidak ditemukan!", idField.getText()),
                            "Warning",
                            MyDialogBox.WARNING_MESSAGE);
                } else {   // jika user ditemukan
                    main.setUser(user);
                    if (user instanceof Anggota) {
                        main.setPanel("anggota");
                    } else {
                        main.setPanel("staf");
                    }
                }
                idField.setText("");
            }
        });

        kembaliBtn.addActionListener(e -> main.setPanel("welcome"));
    }

    @Override
    public void mySetLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        headingTxt.setBorder(BorderFactory.createEmptyBorder(120, 0, 125, 0));
        space.setBorder(BorderFactory.createEmptyBorder(0, 0, 126, 0));

        panelLoginBtn.add(loginBtn);
        panelBackBtn.add(kembaliBtn);
        panelField.add(idField);

        add(headingTxt);
        add(idLbl);
        add(panelField);
        add(space);
        add(panelLoginBtn);
        add(panelBackBtn);
    }

    @Override
    public void setStyle() {
        headingTxt.setHeading();
        idLbl.setSubHeading();
        kembaliBtn.setSecondary();
    }

    @Override
    public void refresh() {
        // ignored
        idField.setText("");
    }
}