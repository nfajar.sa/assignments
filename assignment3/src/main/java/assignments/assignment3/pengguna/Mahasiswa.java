package assignments.assignment3.pengguna;
import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Peminjaman;

public class Mahasiswa extends Anggota{
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    public static final int BATAS_JUMLAH_PEMINJAMAN_BUKU = 3;
    public static final long BATAS_MAKSIMAL_DENDA = 5000;
    private final String tanggalLahir;
    private final String programStudi;
    private final String angkatan;

    public Mahasiswa(String nama, String tanggalLahir, String programStudi, String angkatan) {
        super(nama);
        this.tanggalLahir = tanggalLahir;
        this.programStudi = programStudi;
        this.angkatan = angkatan;
        this.setId(generateId());
    }

    @Override
    public String pinjam(Buku buku, String tanggalPeminjaman) {
        if (this.hitungJumlahPinjaman() == BATAS_JUMLAH_PEMINJAMAN_BUKU) {
            return "Jumlah buku yang sedang dipinjam sudah mencapai batas maksimal";
        } else if (this.denda >= BATAS_MAKSIMAL_DENDA) {
            return "Denda lebih dari Rp5000";
        } else if (this.cekDaftarPeminjaman(buku)) {
            return "Buku " + buku.getJudul() + " oleh " + buku.getPenulis() + " sedang dipinjam";
        }
        buku.setStok(buku.getStok() - 1);
        this.addDaftarPeminjaman(new Peminjaman(this, buku, tanggalPeminjaman));
        buku.addDaftarPeminjam(this);
        return this.getNama() + " berhasil meminjam Buku " + buku.getJudul() + "!";
    }

    @Override
    protected String generateId() {
        return IdGenerator.generateId(this.programStudi,this.angkatan,this.tanggalLahir);
    }
}
