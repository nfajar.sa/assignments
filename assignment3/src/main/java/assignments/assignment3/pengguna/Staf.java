package assignments.assignment3.pengguna;

public class Staf extends Pengguna {
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    private static int jumlahStaf;

    public Staf(String nama) {
        super(nama);
        jumlahStaf += 1;
        this.setId(generateId());
    }

    @Override
    public String toString() {
        return "ID Staf: " + getId()
                + "\nNama Staf: " + getNama();
    }

    @Override
    protected String generateId() {
        return "STAF-" + jumlahStaf;
    }
}
