package assignments.assignment3.pengguna;
import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Peminjaman;

public class Dosen extends Anggota{
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    public static final int BATAS_JUMLAH_PEMINJAMAN_BUKU = 5;
    public static final long BATAS_MAKSIMAL_DENDA = 10000;
    private static int jumlahDosen;

    public Dosen(String nama) {
        super(nama);
        jumlahDosen += 1;   // Bertambah setiap inisiasi
        this.setId(generateId());
    }

    @Override
    public String pinjam(Buku buku, String tanggalPeminjaman) {
        if (this.hitungJumlahPinjaman() == BATAS_JUMLAH_PEMINJAMAN_BUKU) {
            return "Jumlah buku yang sedang dipinjam sudah mencapai batas maksimal";
        } else if (this.denda >= BATAS_MAKSIMAL_DENDA) {
            return "Denda lebih dari Rp10000";
        } else if (this.cekDaftarPeminjaman(buku)) {
            return "Buku " + buku.getJudul() + " oleh " + buku.getPenulis() + " sedang dipinjam";
        }
        buku.setStok(buku.getStok() - 1);
        this.addDaftarPeminjaman(new Peminjaman(this, buku, tanggalPeminjaman));
        buku.addDaftarPeminjam(this);
        return this.getNama() + " berhasil meminjam Buku " + buku.getJudul() + "!";
    }

    @Override
    protected String generateId() {
        return "DOSEN-" + jumlahDosen;
    }
}
