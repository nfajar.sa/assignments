package assignments.assignment3.pengguna;
import assignments.assignment3.buku.Buku;
import assignments.assignment3.buku.Peminjaman;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Anggota extends Pengguna implements CanBorrow, Comparable<Anggota>{
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    protected long denda;
    protected int poin;
    protected Peminjaman[] daftarPeminjaman;

    public Anggota(String nama) {
        super(nama);
    }

    @Override
    public String toString() {
        return "ID Anggota: " + this.getId()
                + "\nNama Anggota: " + this.getNama()
                + "\nTotal Poin: " + this.getPoin()
                + "\nDenda: Rp" + this.getDenda();
    }

    @Override
    public int compareTo(Anggota other) {
        if (this.poin - other.getPoin() == 0){  // Jika poin sama
            return this.getNama().compareTo(other.getNama());   // Berdasarkan alfabetikal A-Z
        }
        return  other.getPoin() - this.poin;    // Berdasarkan poin terbesar
    }

    public void detail(){
        System.out.println(this);
        System.out.println("Riwayat Peminjaman Buku:");
        if (daftarPeminjaman == null) {
            System.out.println("Belum pernah meminjam buku");
        } else {
            int pointer = 1;
            for (Peminjaman peminjaman :
                    daftarPeminjaman) {
                System.out.println("----------------- " + (pointer++) + " -----------------");
                System.out.println(peminjaman);
            }
        }
    }

    public String bayarDenda(long jumlahBayar){
        if (this.denda == 0) {
            return this.getNama() + " tidak memiliki denda";
        } else if (this.denda > jumlahBayar) {  // Kalau denda masih belum lunas
            long sisaDenda = this.denda - jumlahBayar;
            this.setDenda(sisaDenda);
            return this.getNama() + " berhasil membayar denda sebesar Rp" + jumlahBayar + "\nSisa denda saat ini: Rp" + sisaDenda;
        } else {
            long kembalian = jumlahBayar - this.denda;
            this.setDenda(0);
            return this.getNama() + " berhasil membayar lunas denda" + "\nJumlah kembalian: Rp" + kembalian;
        }
    }

    @Override
    public String kembali(Buku buku, String tanggalPengembalian) {
        Peminjaman tempPeminjaman = this.getBukuPinjaman(buku);
        if (tempPeminjaman == null) {
            return "Buku " + buku.getJudul() + " tidak sedang dipinjam oleh " + this.getNama();
        }
        tempPeminjaman.kembalikanBuku(tanggalPengembalian);
        return "Buku " + buku.getJudul() + " berhasil dikembalikan oleh " + this.getNama() + " dengan denda Rp" + tempPeminjaman.hitungDenda() +"!";
    }

    private Peminjaman getBukuPinjaman(Buku buku){
        if (daftarPeminjaman != null) {
            for (Peminjaman bukuPinjam:
                    daftarPeminjaman) {
                if (bukuPinjam.getBuku().equals(buku) && bukuPinjam.getTanggalPengembalian().equals("-")) {
                    return bukuPinjam;
                }
            }
        }
        return null;
    }

    protected int hitungJumlahPinjaman(){
        if (daftarPeminjaman == null) {
            return 0;
        }
        int counter = 0;
        for (Peminjaman p:
             daftarPeminjaman) {
            if (p.getTanggalPengembalian().equals("-")) {   // Cek buku yang belum dikembalikan (sedang dipinjam)
                counter+=1;
            }
        }
        return counter;
    }

    /**
    * Method untuk mengecek suatu buku sedang dipinjam atau tidak
    * @param    buku
    * @return   true, jika sedang dipinjam
    *           false, jika tidak sedang dipinjam
    * */
    protected boolean cekDaftarPeminjaman(Buku buku){
        if (daftarPeminjaman != null) {
            for (Peminjaman bukuPinjam:
                    daftarPeminjaman) {
                if (bukuPinjam.getBuku().equals(buku) && bukuPinjam.getTanggalPengembalian().equals("-")){
                    return true;
                }
            }
        }
        return false;
    }
    protected void addDaftarPeminjaman(Peminjaman peminjaman) {
        ArrayList<Peminjaman> tempDaftarPeminjaman;
        if (daftarPeminjaman == null) {
            tempDaftarPeminjaman = new ArrayList<>();
        } else {
            tempDaftarPeminjaman = new ArrayList<>(Arrays.asList(daftarPeminjaman));
        }
        tempDaftarPeminjaman.add(peminjaman);
        daftarPeminjaman = tempDaftarPeminjaman.toArray(new Peminjaman[0]);
    }

    public long getDenda() {
        return denda;
    }

    public void setDenda(long denda) {
        this.denda = denda;
    }

    public int getPoin() {
        return poin;
    }

    public void setPoin(int poin) {
        this.poin = poin;
    }
}
