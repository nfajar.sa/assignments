package assignments.assignment3.pengguna;

public abstract class Pengguna {
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    private String id;
    private final String nama;

    public Pengguna(String nama) {
        this.nama = nama;
    }

    @Override
    abstract public String toString();
    abstract protected String generateId();

    public String getId() {
        return id;
    }
    public String getNama() {
        return nama;
    }
    public void setId(String id) {
        this.id = id;
    }
}
