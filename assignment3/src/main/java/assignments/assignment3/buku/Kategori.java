package assignments.assignment3.buku;

public class Kategori {
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    private final String nama;
    private final int poin;

    public Kategori(String nama, int poin){
        this.nama = nama;
        this.poin = poin;
    }

    @Override
    public String toString() {
        return "Kategori: " + this.nama +
                "\nPoin: " + this.poin;
    }

    public String getNama(){
        return nama;
    }

    public int getPoin(){
        return poin;
    }
}
