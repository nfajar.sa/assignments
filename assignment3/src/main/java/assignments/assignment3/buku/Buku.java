package assignments.assignment3.buku;
import assignments.assignment3.pengguna.CanBorrow;
import java.util.ArrayList;
import java.util.Arrays;

public class Buku {
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    private final String judul;
    private final String penulis;
    private final String penerbit;
    private final Kategori kategori;
    private final int stokAwal;
    private int stok;
    private CanBorrow[] daftarPeminjam;

    public Buku(String judul, String penulis, String penerbit, Kategori kategori, int stok) {
        this.judul = judul;
        this.penulis = penulis;
        this.penerbit = penerbit;
        this.kategori = kategori;
        this.stok = stok;
        this.stokAwal = stok;
    }

    @Override
    public String toString() {
        return "Judul Buku: " + this.judul +
                "\nPenulis Buku: " + this.penulis +
                "\nPenerbit Buku: " + this.penerbit +
                "\n" + this.kategori;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Buku buku = (Buku) o;
        return judul.equalsIgnoreCase(buku.judul) && penulis.equalsIgnoreCase(buku.penulis);
    }

    public void daftarPeminjam(){
        System.out.println(this);
        System.out.println("---------- Daftar Peminjam ----------");
        if (daftarPeminjam == null) {
            System.out.println("Belum ada anggota yang meminjam buku " + this.judul);
        } else {
            int pointer = 1;
            for (CanBorrow peminjam :
                    daftarPeminjam) {
                System.out.println("----------------- " + (pointer++) + " -----------------");
                System.out.println(peminjam);
            }
        }
    }

    public void addDaftarPeminjam(CanBorrow peminjam){
        ArrayList<CanBorrow> tempDaftarPeminjam;
        if (daftarPeminjam == null) {   // Jika belum ada yang meminjam buku ini
            tempDaftarPeminjam = new ArrayList<>();
        } else {
            tempDaftarPeminjam = new ArrayList<>(Arrays.asList(daftarPeminjam));
        }
        tempDaftarPeminjam.add(peminjam);
        daftarPeminjam = tempDaftarPeminjam.toArray(new CanBorrow[0]);
    }

    public String getJudul() {
        return judul;
    }

    public String getPenulis() {
        return penulis;
    }

    public int getStok() {
        return stok;
    }

    public Kategori getKategori() {
        return kategori;
    }

    public void setStok(int newStock){
        this.stok = newStock;
    }

    public int getStokAwal() {
        return stokAwal;
    }
}