package assignments.assignment3.buku;
import assignments.assignment3.pengguna.Anggota;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Peminjaman {
    // TODO: Implementasi kelas ini sesuai dengan UML Diagram (attribute, method, inheritance, dll)
    private static final long DENDA_PER_HARI = 3000;
    private final Anggota anggota;
    private final Buku buku;
    private final String tanggalPeminjaman;
    private String tanggalPengembalian = "-";
    private long denda;

    @Override
    public String toString() {
        return this.buku +
                "\nTanggal Peminjaman: " + this.tanggalPeminjaman +
                "\nTanggal Pengembalian: " + this.tanggalPengembalian +
                "\nDenda: Rp" + this.denda;
    }

    public Peminjaman(Anggota anggota, Buku buku, String tanggalPeminjaman) {
        this.anggota = anggota;
        this.buku = buku;
        this.tanggalPeminjaman = tanggalPeminjaman;
    }

    public void kembalikanBuku(String tanggalPengembalian){
        setTanggalPengembalian(tanggalPengembalian);
        buku.setStok(buku.getStok() + 1);
        setDendaPeminjaman(hitungDenda());
        anggota.setPoin(anggota.getPoin() + buku.getKategori().getPoin());
    }

    public long hitungDenda(){
        long durasi = hitungDurasi();
        long tempDenda = 0;
        if (durasi > 7) {
            tempDenda = (durasi-7) * DENDA_PER_HARI;
        }
        return tempDenda;
    }

    private long hitungDurasi(){
        if (!tanggalPeminjaman.equals("-")){    // Safe check
            Calendar tempLoan = Calendar.getInstance();
            Calendar tempReturn = Calendar.getInstance();
            // Array [Hari, Bulan, Tahun]
            int[] arrLoan = getTanggal(tanggalPeminjaman);
            int[] arrReturn = getTanggal(tanggalPengembalian);

            tempLoan.set(arrLoan[2], arrLoan[1], arrLoan[0]);
            tempReturn.set(arrReturn[2], arrReturn[1], arrReturn[0]);
            // Cari durasi/selisih antara kedua tanggal
            long compare = tempReturn.getTimeInMillis() - tempLoan.getTimeInMillis();
            return TimeUnit.MILLISECONDS.toDays(compare);
        }
        return 0;
    }

    private static int[] getTanggal(String dateFormat){
        String[] date = dateFormat.split("/");
        int[] finalDate = new int[3];
        int index = 0;
        for (String num:
                date) {
            finalDate[index++] = Integer.parseInt(num);
        }
        return finalDate;
    }

    public Buku getBuku() {
        return buku;
    }

    public String getTanggalPengembalian() {
        return tanggalPengembalian;
    }

    public void setTanggalPengembalian(String tanggalPengembalian) {
        this.tanggalPengembalian = tanggalPengembalian;
    }

    public void setDendaPeminjaman(long denda){
        this.denda += denda;
        anggota.setDenda(anggota.getDenda() + denda);
    }

}
