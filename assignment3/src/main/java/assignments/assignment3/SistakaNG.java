package assignments.assignment3;
import assignments.assignment3.buku.*;
import assignments.assignment3.pengguna.*;
import java.util.*;

public class SistakaNG {
    private static final Scanner input = new Scanner(System.in);

    public static Pengguna penggunaLoggedIn;
    public static Staf[] daftarStaf;
    public static Anggota[] daftarAnggota;
    public static Buku[] daftarBuku;
    public static Kategori[] daftarKategori;


    public static void main(String[] args) {
        System.out.println("Start - Register Staf...");
        registerStaf();
        System.out.println("Done - Register Staf...\n");
        menu();
        input.close();
    }

    // Method ini digunakan untuk mendaftarkan staf-staf ketika program dijalankan
    private static void registerStaf() {
        String[] listNama = new String[]{"Dek Depe", "Dek DePram", "Dek Sofita", "Winter", "Boo"};

        for (int i = 0; i < listNama.length; i++) {
            // TODO: Buat objek Staf menggunakan listNama[i]
            Staf staf = new Staf(listNama[i]);
            ArrayList<Staf> tempDaftarStaf;
            if (daftarStaf == null) {
                tempDaftarStaf = new ArrayList<>();
            } else {
                tempDaftarStaf = new ArrayList<>(Arrays.asList(daftarStaf));
            }
            tempDaftarStaf.add(staf);
            daftarStaf = tempDaftarStaf.toArray(new Staf[0]);

            // TODO: Setelah objek Staf behasil dibuat, uncomment 2 baris kode di bawah ini
             System.out.println("Berhasil menambahkan staf dengan data:");
             System.out.println(staf);
        }
    }

    // Method ini digunakan untuk mencetak menu utama dari SistakaNG
    public static void menu() {
        boolean hasChosenExit = false;
        System.out.println("Selamat Datang di Sistem Perpustakaan SistakaNG!");
        while (!hasChosenExit) {
            int command = 0;
            System.out.println("================ Menu Utama ================\n");
            System.out.println("1. Login");
            System.out.println("2. Keluar");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();
            if (command == 1) {
                menuLogin();
            } else if (command == 2) {
                System.out.println("Terima kasih telah menggunakan SistakaNG. Sampai jumpa di lain kesempatan!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }
    }

    // Method ini digunakan untuk mencetak menu login
    public static void menuLogin() {
        boolean isLoginSuccess = false;
        while (!isLoginSuccess) {
            System.out.println("Masukkan ID Anda untuk login ke sistem");
            System.out.print("ID: ");
            String id = input.nextLine();

            // TODO: Implementasi login -> jika login berhasil, ubah nilai isLoginSuccess menjadi true
            penggunaLoggedIn = getPengguna(id);
            if (penggunaLoggedIn != null) {
                isLoginSuccess = true;
                System.out.println("Halo, " + penggunaLoggedIn.getNama() +"! Selamat datang di SistakaNG");
            } else {
                System.out.println("Pengguna dengan ID "+ id +" tidak ditemukan");
            }
        }

        showMenu();
    }

    private static Pengguna getPengguna(String idPengguna){
        if (idPengguna.startsWith("STAF-")) {
            if (daftarStaf != null) {
                for (Staf staf:
                        daftarStaf) {
                    if (staf.getId().equals(idPengguna)) {
                        return staf;
                    }
                }
            }
        } else {
            if (daftarAnggota != null){
                for (Anggota anggota:
                     daftarAnggota) {
                    if (anggota.getId().equals(idPengguna)) {
                        return anggota;
                    }
                }
            }
        }
        return null;

    }

    // Method ini digunakan untuk mencetak menu yang dapat diakses berdasarkan jenis penggunaLoggedIn
    private static void showMenu() {
        if (penggunaLoggedIn instanceof Staf) {
            showMenuStaf();
        } else {
            showMenuAnggota();
        }
    }

    // Method ini digunakan untuk mencetak menu staf dari SistakaNG
    private static void showMenuStaf() {
        int command = 0;
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println("================ Menu Staf ================\n");
            System.out.println("1. Tambah Anggota");
            System.out.println("2. Tambah Kategori");
            System.out.println("3. Tambah Buku");
            System.out.println("4. Hapus Buku");
            System.out.println("5. 3 Peringkat Pertama");
            System.out.println("6. Detail Anggota");
            System.out.println("7. Daftar Peminjam Buku");
            System.out.println("99. Logout");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();
            if (command == 1) {
                tambahAnggota();
            } else if (command == 2) {
                tambahKategori();
            } else if (command == 3) {
                tambahBuku();
            } else if (command == 4) {
                hapusBuku();
            } else if (command == 5) {
                peringkat();
            } else if (command == 6) {
                detailAnggota();
            } else if (command == 7) {
                daftarPeminjamBuku();
            } else if (command == 99) {
                System.out.println("Terima kasih telah menggunakan SistakaNG!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }
    }

    // Method ini digunakan untuk mencetak menu anggota dari SistakaNG
    private static void showMenuAnggota() {
        int command = 0;
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println("================ Menu Anggota ================\n");
            System.out.println("1. Peminjaman");
            System.out.println("2. Pengembalian");
            System.out.println("3. Pembayaran Denda");
            System.out.println("4. Detail Anggota");
            System.out.println("99. Logout");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();
            if (command == 1) {
                peminjaman();
            } else if (command == 2) {
                pengembalian();
            } else if (command == 3) {
                pembayaranDenda();
            } else if (command == 4) {
                ((Anggota) penggunaLoggedIn).detail();
            } else if (command == 99) {
                System.out.println("Terima kasih telah menggunakan SistakaNG!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }
    }

    // ================= MENU STAF =================

    private static void tambahAnggota(){
        System.out.println("---------- Tambah Anggota ----------");
        System.out.print("Tipe Anggota: ");
        String tipeAnggota = input.nextLine();

        if (tipeAnggota.equals("Mahasiswa")) {
            tambahMahasiswa();
        } else if (tipeAnggota.equals("Dosen")) {
            tambahDosen();
        } else {
            System.out.println("Tipe Anggota " + tipeAnggota + " tidak valid!");
        }
    }

    private static void tambahMahasiswa(){
        System.out.print("Nama: ");
        String nama = input.nextLine();
        System.out.print("Program Studi (SIK/SSI/MIK/MTI/DIK): ");
        String programStudi = input.nextLine();
        System.out.print("Angkatan: ");
        String angkatan = input.nextLine();
        System.out.print("Tanggal Lahir (dd/mm/yyyy): ");

        String tanggalLahir = input.nextLine();
        Mahasiswa tempMahasiswa = new Mahasiswa(nama, tanggalLahir, programStudi, angkatan);

        if (tempMahasiswa.getId().equals("Input tidak valid!")) {
            System.out.println("Tidak dapat menambahkan anggota silahkan periksa kembali input anda!");
        } else {
            setDaftarAnggota(tempMahasiswa);
            System.out.println("Berhasil menambahkan mahasiswa dengan data:");
            System.out.println(tempMahasiswa);
        }
    }

    private static void tambahDosen(){
        System.out.print("Nama: ");
        String nama = input.nextLine();

        Dosen tempDosen = new Dosen(nama);
        setDaftarAnggota(tempDosen);
        System.out.println("Berhasil menambahkan dosen dengan data:");
        System.out.println(tempDosen);
    }

    private static void tambahKategori(){
        System.out.println("---------- Tambah Kategori ----------");
        System.out.print("Nama Kategori: ");
        String nama = input.nextLine();
        System.out.print("Poin: ");
        int poin = Integer.parseInt(input.nextLine());

        Kategori tempKategori = new Kategori(nama, poin);
        Kategori cekKategori = getKategori(nama);

        if (cekKategori != null) {
            System.out.println("Kategori " + cekKategori.getNama() + " sudah pernah ditambahkan");
        } else {
            setDaftarKategori(tempKategori);
            System.out.println("Kategori " + tempKategori.getNama() + " dengan poin " + tempKategori.getPoin() + " berhasil ditambahkan");
        }
    }

    private static void tambahBuku(){
        System.out.println("---------- Tambah Buku ----------");
        System.out.print("Judul: ");
        String judul = input.nextLine();
        System.out.print("Penulis: ");
        String penulis = input.nextLine();
        System.out.print("Penerbit: ");
        String penerbit = input.nextLine();
        System.out.print("Kategori: ");
        String kategori = input.nextLine();
        System.out.print("Stok: ");
        int stok = Integer.parseInt(input.nextLine());
        
        Buku tempBuku = getBuku(judul, penulis);
        Kategori tempKategori = getKategori(kategori);

        if (tempKategori == null) {
            System.out.println("Kategori " + kategori + " tidak ditemukan");
        } else if (stok <= 0) {
            System.out.println("Stok harus lebih dari 0");
        } else if (tempBuku != null) {
            System.out.println("Buku " + tempBuku.getJudul() + " oleh " + tempBuku.getPenulis() + " sudah pernah ditambahkan");
        } else {
            Buku bukuBaru = new Buku(judul, penulis, penerbit, tempKategori, stok);
            setDaftarBuku(bukuBaru);
            System.out.println("Buku " + bukuBaru.getJudul() + " oleh " + bukuBaru.getPenulis() + " berhasil ditambahkan");
        }
    }
    
    private static void hapusBuku(){
        System.out.println("---------- Hapus Buku ----------");
        System.out.print("Judul: ");
        String judul = input.nextLine();
        System.out.print("Penulis: ");
        String penulis = input.nextLine();

        Buku tempBuku = getBuku(judul, penulis);

        if (tempBuku == null) {
            System.out.println("Buku " + judul + " oleh " + penulis + " tidak ditemukan");
        } else if (tempBuku.getStok() < tempBuku.getStokAwal()) {
            System.out.println("Buku " + judul + " oleh " + penulis + " tidak dapat dihapus karena sedang dipinjam");
        } else {
            ArrayList<Buku> tempDaftarBuku = new ArrayList<>(Arrays.asList(daftarBuku));
            tempDaftarBuku.remove(tempBuku);
            daftarBuku = tempDaftarBuku.toArray(new Buku[0]);
            System.out.println("Buku " + tempBuku.getJudul() + " oleh " + tempBuku.getPenulis() + " berhasil dihapus");
        }
    }

    private static void peringkat(){
        System.out.println("---------- Peringkat Anggota ----------");
        if (daftarAnggota == null) {
            System.out.println("Belum ada anggota yang terdaftar pada sistem");
        } else {
            List<Anggota> tempDaftarAnggota =  new ArrayList<>(Arrays.asList(daftarAnggota));
            if (daftarAnggota.length > 1) {
                Collections.sort(tempDaftarAnggota);
                if (tempDaftarAnggota.size() > 3) {
                    tempDaftarAnggota = tempDaftarAnggota.subList(0,3); // Ambil 3 peringkat teratas
                }
            }
            int pointer = 1;
            for (Anggota anggota:
                    tempDaftarAnggota) {
                System.out.println("----------------- " + (pointer++) + " -----------------");
                System.out.println(anggota);
            }
        }

    }

    private static void detailAnggota(){
        System.out.println("---------- Detail Anggota ----------");
        System.out.print("ID Anggota: ");
        String idAnggota = input.nextLine();

        Anggota tempAnggota = getAnggota(idAnggota);
        if (tempAnggota == null) {
            System.out.println("Anggota dengan ID " + idAnggota + " tidak ditemukan");
        } else {
            tempAnggota.detail();
        }
    }

    private static void daftarPeminjamBuku(){
        System.out.println("---------- Daftar Peminjam Buku ----------");
        System.out.print("Judul: ");
        String judul = input.nextLine();
        System.out.print("Penulis: ");
        String penulis = input.nextLine();

        Buku tempBuku = getBuku(judul, penulis);
        if (tempBuku == null) {
            System.out.println("Buku " + judul + " oleh " + penulis + " tidak ditemukan");
        } else {
            tempBuku.daftarPeminjam();
        }
    }

    // ================= MENU ANGGOTA =================

    private static void peminjaman(){
        System.out.println("---------- Peminjaman Buku ----------");
        System.out.print("Judul Buku: ");
        String judulBuku = input.nextLine();
        System.out.print("Penulis Buku: ");
        String penulisBuku = input.nextLine();
        System.out.print("Tanggal Peminjaman: ");
        String tanggalPeminjaman = input.nextLine();

        Buku tempBuku = getBuku(judulBuku, penulisBuku);

        if (tempBuku == null) {
            System.out.println("Buku " + judulBuku + " oleh " + penulisBuku + " tidak ditemukan");
        } else if (tempBuku.getStok() <= 0) {
            System.out.println("Buku " + judulBuku + " oleh " + penulisBuku + " tidak tersedia");
        } else {
            System.out.println(((Anggota) penggunaLoggedIn).pinjam(tempBuku, tanggalPeminjaman));
        }
    }

    private static void pengembalian(){
        System.out.println("---------- Pengembalian Buku ----------");
        System.out.print("Judul Buku: ");
        String judulBuku = input.nextLine();
        System.out.print("Penulis Buku: ");
        String penulisBuku = input.nextLine();
        System.out.print("Tanggal Pengembalian: ");
        String tanggalPengembalian = input.nextLine();

        Buku tempBuku = getBuku(judulBuku, penulisBuku);
        if (tempBuku == null) {
            System.out.println("Buku " + judulBuku + " oleh " + penulisBuku + " tidak ditemukan");
        } else {
            System.out.println(((Anggota) penggunaLoggedIn).kembali(tempBuku, tanggalPengembalian));
        }
    }

    private static void pembayaranDenda(){
        System.out.println("---------- Pembayaran Denda ----------");
        System.out.print("Jumlah: ");
        long jumlah = Long.parseLong(input.nextLine());
        System.out.println(((Anggota) penggunaLoggedIn).bayarDenda(jumlah));
    }

    // ================= GETTER SETTER =================

    private static void setDaftarAnggota(Anggota anggotaBaru){
        ArrayList<Anggota> tempDaftarAnggota;
        if (daftarAnggota == null) {
            tempDaftarAnggota = new ArrayList<>();
        } else {
            tempDaftarAnggota = new ArrayList<>(Arrays.asList(daftarAnggota));
        }
        tempDaftarAnggota.add(anggotaBaru);
        daftarAnggota = tempDaftarAnggota.toArray(new Anggota[0]);
    }

    private static void setDaftarKategori(Kategori kategoriBaru) {
        ArrayList<Kategori> tempDaftarKategori;
        if (daftarKategori == null) {
            tempDaftarKategori = new ArrayList<>();
        } else {
            tempDaftarKategori = new ArrayList<>(Arrays.asList(daftarKategori));
        }
        tempDaftarKategori.add(kategoriBaru);
        daftarKategori = tempDaftarKategori.toArray(new Kategori[0]);
    }

    private static Kategori getKategori(String nama){
        if (daftarKategori != null) {
            // Iterasi untuk mencari nama kategori yang sama
            for (Kategori c:
                    daftarKategori) {
                if (c.getNama().equalsIgnoreCase(nama)) {
                    return c;
                }
            }
        }
        return null;
    }

    private static void setDaftarBuku(Buku bukuBaru) {
        ArrayList<Buku> tempDaftarBuku;
        if (daftarBuku == null) {
            tempDaftarBuku = new ArrayList<>();
        } else {
            tempDaftarBuku = new ArrayList<>(Arrays.asList(daftarBuku));
        }
        tempDaftarBuku.add(bukuBaru);
        daftarBuku= tempDaftarBuku.toArray(new Buku[0]);
    }

    private static Buku getBuku(String judul, String penulis){
        if (daftarBuku != null) {
            for (Buku b:
                    daftarBuku) {
                if (b.getJudul().equalsIgnoreCase(judul) && b.getPenulis().equalsIgnoreCase(penulis)){
                    return b;
                }
            }
        }
        return null;
    }

    private static Anggota getAnggota(String idAnggota) {
        // Iterasi untuk mencari idAnggota yang sama
        if(daftarAnggota != null){
            for (Anggota a:
                    daftarAnggota) {
                if (a.getId().equals(idAnggota)) {
                    return a;
                }
            }
        }
        return null;
    }
}