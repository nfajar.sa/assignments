# Tugas Pemrograman 2: Sistem Perpustakaan

### Day 1 : 22 Maret 2022
- Baca Soal
- Bikin kerangka method
- Tambahin class IdGenerator

### Day 2 : 23 Maret 2022
- Modifikasi class IdGenerator
- Build method addAnggota 
- Build method setMembers
- Build method addKategori
- Build method getter-setter categories
- Build method addBook
- Build method getter setter books
- Build constructor 

### Day 3 : 24 Maret 2022
- Build Method pinjamBuku & kembalikanBuku
- Build Method bayarDenda & detailAnggota
- Build Method peringkatAnggota

### Day 4 : 25 Maret 2022
- Create bubble sort alfabetical
- Clean Code
- Fix Bug
- Add Comments