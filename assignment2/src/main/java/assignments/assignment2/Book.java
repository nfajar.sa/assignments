package assignments.assignment2;

// TODO
public class Book {
    private final String title;
    private final String author;
    private final String publisher;
    private final Category category;
    private int stock;

    public Book(String title, String author, String publisher, Category category, int stok) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.category = category;
        this.stock = stok;
    }

    @Override
    public String toString() {
        // TODO
        return "Judul Buku: " + this.title +
                "\nPenulis Buku: " + this.author +
                "\nPenerbit Buku: " + this.publisher +
                "\n" + this.category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return title.equalsIgnoreCase(book.title) && author.equalsIgnoreCase(book.author);
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getStock() {
        return stock;
    }

    public Category getCategory() {
        return category;
    }

    public void setStock(int newStock){
        this.stock = newStock;
    }
}
