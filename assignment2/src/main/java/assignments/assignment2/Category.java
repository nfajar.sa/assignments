package assignments.assignment2;

// TODO
public class Category {
    private final String name;
    private final int point;

    public Category(String name, int point){
        this.name = name;
        this.point = point;
    }

    @Override
    public String toString() {
        // TODO
        return "Kategori: " + this.name +
                "\nPoint: " + this.point;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return name.equalsIgnoreCase(category.name);
    }

    public String getName(){
        return name;
    }

    public int getPoint(){
        return point;
    }
}
