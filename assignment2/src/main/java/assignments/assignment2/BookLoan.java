package assignments.assignment2;

public class BookLoan {
    private static final long DENDA_PER_HARI = 3000;
    private final Member member;
    private final Book book;
    private final String loanDate;
    private String returnDate;
    private long fine;
    private boolean status;

    public BookLoan(Member member, Book book, String loanDate) {
        this.member = member;
        this.book = book;
        this.loanDate = loanDate;
        this.returnDate = "-";
        this.status = true;
    }

    @Override
    public String toString() {
        return this.book +
                "\nTanggal Peminjaman: " + this.loanDate +
                "\nTanggal Pengembalian: " + this.returnDate +
                "\nDenda: Rp " + this.fine;
    }

    public long getFineLoan(){
        return this.fine;
    }

    public Book getBook() {
        return book;
    }

    public String getLoanDate() {
        return loanDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public boolean getStatus(){
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public void setFineLoan(long duration){
        long tempFine = 0;
        if (duration > 7) {
            tempFine = (duration-7) * DENDA_PER_HARI;
        }
        this.fine += tempFine;
        member.setFine(member.getFine() + tempFine);
    }
}
