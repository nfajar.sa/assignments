package assignments.assignment2;

import java.util.Arrays;
import java.util.Scanner;

public class Library {
    private final Scanner input = new Scanner(System.in);

    private Member[] members;
    private Book[] books;
    private Category[] categories;

    public static void main(String[] args) {
        Library program = new Library();
        program.menu();
    }

    public void menu() {
        int command;
        boolean hasChosenExit = false;
        System.out.println("Selamat Datang di Sistem Perpustakaan SistakaNG!");
        while (!hasChosenExit) {
            System.out.println("================ Menu Utama ================\n");
            System.out.println("1. Tambah Anggota");
            System.out.println("2. Tambah Kategori");
            System.out.println("3. Tambah Buku");
            System.out.println("4. Peminjaman");
            System.out.println("5. Pengembalian");
            System.out.println("6. Pembayaran Denda");
            System.out.println("7. Detail Anggota");
            System.out.println("8. 3 Peringkat Pertama");
            System.out.println("99. Keluar");
            System.out.print("Masukkan pilihan menu: ");
            command = Integer.parseInt(input.nextLine());
            System.out.println();
            if (command == 1) {
                addAnggota();
            } else if (command == 2) {
                addKategori();
            } else if (command == 3) {
                addBuku();
            } else if (command == 4) {
                pinjamBuku();
            } else if (command == 5) {
                kembalikanBuku();
            } else if (command == 6) {
                bayarDenda();
            } else if (command == 7) {
                detailAnggota();
            } else if (command == 8) {
                peringkatAnggota();
            } else if (command == 99) {
                System.out.println("Terima kasih telah menggunakan SistakaNG!");
                hasChosenExit = true;
            } else {
                System.out.println("Menu tidak dikenal!");
            }
            System.out.println();
        }

        input.close();
    }

    /**
     * Method menu tambah anggota
     * */
    private void addAnggota(){
        System.out.println("---------- Tambah Anggota ----------");
        System.out.print("Nama: ");
        String nama = input.nextLine();
        System.out.print("Program Studi (SIK/SSI/MIK/MTI/DIK): ");
        String programStudi = input.nextLine();
        System.out.print("Angkatan: ");
        String angkatan = input.nextLine();
        System.out.print("Tanggal Lahir (dd/mm/yyyy): ");
        String tanggalLahir = input.nextLine();

        String idAnggota = IdGenerator.generateId(programStudi, angkatan, tanggalLahir);
        if (idAnggota.equals("Input tidak valid!")) {
            System.out.println("Tidak dapat menambahkan anggota silahkan periksa kembali input anda!");
        } else {
            Member newMember = new Member(idAnggota, nama, tanggalLahir, programStudi, angkatan);
            setMembers(newMember);
            System.out.println("Member "+ newMember.getNama() +" berhasil ditambahkan dengan data:");
            System.out.println(newMember);
        }
    }

    /**
     * Method menu tambah kategori
     * */
    private void addKategori(){
        // TODO : Add Comment
        System.out.println("---------- Tambah Kategori ----------");
        System.out.print("Nama Kategori: ");
        String nama = input.nextLine();
        System.out.print("Point: ");
        int point = Integer.parseInt(input.nextLine());

        Category newCategory = new Category(nama,point);
        Category checkCategory = getCategory(nama);
        if (checkCategory != null) {
            System.out.println("Kategori " + checkCategory.getName() + " sudah pernah ditambahkan");
        } else {
            setCategories(newCategory);
            System.out.println("Kategori " + newCategory.getName() + " dengan " + newCategory.getPoint() + " point berhasil ditambahkan");
        }
    }

    /**
     * Method menu tambah buku
     * */
    private void addBuku(){
        System.out.println("---------- Tambah Buku ----------");
        System.out.print("Judul: ");
        String nama = input.nextLine();
        System.out.print("Penulis: ");
        String penulis = input.nextLine();
        System.out.print("Penerbit: ");
        String penerbit = input.nextLine();
        System.out.print("Kategori: ");
        String kategori = input.nextLine();
        System.out.print("Stok: ");
        int stok = Integer.parseInt(input.nextLine());

        Book tempBook = getBook(nama, penulis);
        Category tempCategory = getCategory(kategori);
        if (tempBook != null) {
            System.out.println("Buku " + tempBook.getTitle() + " oleh " + tempBook.getAuthor() + " sudah pernah ditambahkan");
        } else if (tempCategory == null){
            System.out.println("Kategori " + kategori + " tidak ditemukan");
        } else if (stok <= 0){
            System.out.println("Stok harus lebih dari 0");
        } else {
            Book newBook = new Book(nama, penulis, penerbit, tempCategory, stok);
            setBooks(newBook);
            System.out.println("Buku " + newBook.getTitle() + " oleh " + newBook.getAuthor() + " berhasil ditambahkan");
        }
    }

    /**
     * Method menu peminjaman buku
     * */
    private void pinjamBuku() {
        // TODO
        System.out.println("---------- Peminjaman Buku ----------");
        System.out.print("ID Anggota: ");
        String idAnggota = input.nextLine();
        System.out.print("Judul Buku: ");
        String judulBuku = input.nextLine();
        System.out.print("Penulis Buku: ");
        String penulisBuku = input.nextLine();
        System.out.print("Tanggal Peminjaman: ");
        String tanggalPeminjaman = input.nextLine();

        Member tempMember = getMembers(idAnggota);
        Book tempBook = getBook(judulBuku, penulisBuku);
        if (tempMember == null) {
            System.out.println("Anggota dengan ID " + idAnggota + " tidak ditemukan");
        } else if (tempBook == null){
            System.out.println("Buku " + judulBuku + " oleh " + penulisBuku + " tidak ditemukan");
        } else if (tempBook.getStock() <= 0) {
            System.out.println("Buku " + judulBuku + " oleh " + penulisBuku + " tidak tersedia");
        } else if ((tempMember.getBookLoan() != null) && (countTrueLoan(tempMember.getBookLoan()) >= 3)) {
            System.out.println("Jumlah buku yang sedang dipinjam sudah mencapai batas maksimal");
        } else if (tempMember.getFine() >= 5000){ // lebih dari sama dengan harusnya SKSK
            System.out.println("Denda lebih dari Rp 5000");
        } else if (checkBookLoan(tempMember.getBookLoan(), tempBook)){
            System.out.println("Buku " + judulBuku + " oleh " + penulisBuku + " sedang dipinjam");
        } else {
            tempMember.pinjam(tempBook, tanggalPeminjaman);
            System.out.println(tempMember.getNama() + " berhasil meminjam Buku " + tempBook.getTitle() + "!");
        }
    }

    /**
     * Method menu pengembalian buku
     * */
    private void kembalikanBuku() {
        // TODO
        System.out.println("---------- Pengembalian Buku ----------");
        System.out.print("ID Anggota: ");
        String idAnggota = input.nextLine();
        System.out.print("Judul Buku: ");
        String judulBuku = input.nextLine();
        System.out.print("Penulis Buku: ");
        String penulisBuku = input.nextLine();
        System.out.print("Tanggal Pengembalian: ");
        String tanggalPengembalian = input.nextLine();

        Member tempMember = getMembers(idAnggota);
        Book tempBook = getBook(judulBuku, penulisBuku);
        if (tempMember == null) {
            System.out.println("Anggota dengan ID " + idAnggota + " tidak ditemukan");
        } else if (tempBook == null){
            System.out.println("Buku " + judulBuku + " oleh " + penulisBuku + " tidak ditemukan");
        } else if (!checkBookLoan(tempMember.getBookLoan(), tempBook)){
            System.out.println("Buku " + judulBuku + " tidak sedang dipinjam");
        } else {
            tempMember.kembali(tempBook, tanggalPengembalian);
        }
    }

    /**
     * Method menu pembayaran denda
     * */
    private void bayarDenda() {
        // TODO
        System.out.println("---------- Pembayaran Denda ----------");
        System.out.print("ID Anggota: ");
        String idAnggota = input.nextLine();
        System.out.print("Jumlah: ");
        long jumlah = Long.parseLong(input.nextLine());

        Member tempMember = getMembers(idAnggota);
        if (tempMember == null) {
            System.out.println("Anggota dengan ID " + idAnggota + " tidak ditemukan");
        } else if (tempMember.getFine() <= 0){
            System.out.println(tempMember.getNama() + " tidak memiliki denda");
        } else {
            tempMember.bayarDenda(jumlah);
        }
    }

    /**
     * Method menu detail anggota
     * */
    private void detailAnggota() {
        // TODO
        System.out.println("---------- Detail Anggota ----------");
        System.out.print("ID Anggota: ");
        String idAnggota = input.nextLine();

        Member tempMember = getMembers(idAnggota);
        if (tempMember == null) {
            System.out.println("Anggota dengan ID " + idAnggota + " tidak ditemukan");
        } else {
            tempMember.detail();
        }
    }

    /**
     * Method menu 3 peringkat pertama
     * */
    private void peringkatAnggota() {
        // TODO
        System.out.println("---------- Peringkat Anggota ----------");
        Member[] sortMembers;
        if (members != null) {
            if (members.length > 1) {
                sortMembers = bubbleSort(members);
            } else {
                sortMembers = members;
            }
            int pointer = 1;
            for (Member member:
                    sortMembers) {
                System.out.println("----------------- " + (pointer++) + " -----------------");
                System.out.println(member);
            }
        } else {
            System.out.println("Belum ada anggota yang terdaftar pada sistem");
        }
    }

    /**
     * Method untuk sorting array of member berdasarkan point dan alfabet nama.
     * Method ini diadaptasi dari <a href="https://www.geeksforgeeks.org/bubble-sort/">GeeksForGeeks</a>
     * @param arrayMember   array of member yang akan diurutkan
     * @return              arrayMember yang sudah terurut
     * */
    private Member[] bubbleSort(Member[] arrayMember){
        Member[] tempMembers = Arrays.copyOf(arrayMember, arrayMember.length);
        int len = tempMembers.length;
        int i, j;
        Member temp;
        boolean swapped;
        for (i = 0; i < len ; i++) {
            swapped = false;
            for (j = 0; j < len - i - 1 ; j++) {
                if ((tempMembers[j].getPoint() < tempMembers[j+1].getPoint())   // berdasarkan point
                    || (tempMembers[j].getPoint() == tempMembers[j+1].getPoint() && tempMembers[j].getNama().compareTo(tempMembers[j+1].getNama()) > 0)) {  // berdasarkan alfabet
                    temp = tempMembers[j];
                    tempMembers[j] = tempMembers[j+1];
                    tempMembers[j+1] = temp;
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
        return tempMembers;
    }

    /**
     * Method untuk melakukan cek buku sedang dipinjam atau tidak
     * @param book          Buku yang ingin dicek
     * @param bookLoans     Array of bookloans
     * @return              Return true jika buku yang ingin dicek ada di bookloans dengan status true.
     *                      Return false jika sebaliknya.
     * */
    private boolean checkBookLoan(BookLoan[] bookLoans, Book book){
        if (bookLoans != null) {
            for (BookLoan bookLoan:
                    bookLoans) {
                if (bookLoan.getStatus() && (bookLoan.getBook().equals(book))){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Method untuk menghitung jumlah bookloan dengan status true
     * @param bookLoans     Array of bookloans yang akan dicek
     * @return              Jumlah booklan dengan status true
     * */
    private int countTrueLoan(BookLoan[] bookLoans){
        if (bookLoans != null) {
            int count = 0;
            for (BookLoan bookLoan:
                    bookLoans) {
                if (bookLoan.getStatus()){
                    count++;
                }
            }
            return count;
        }
        return 0;
    }



    private Member getMembers(String idAnggota){
        int getIndex = 0;
        boolean isExist = false;
        // Iterasi untuk mencari idAnggota yang sama
        //null nya kelewat
        if(members != null){
            for (Member m:
                    members) {
                if (m.getId().equals(idAnggota)) {
                    isExist = true;
                    break;
                }
                getIndex++;
            }
        }
        if(isExist){
            return members[getIndex];
        } else {
            return null;
        }

    }

    private Category getCategory(String nameCategory){
        int getIndex = 0;
        boolean isExist = false;
        if (categories != null) {
            // Iterasi untuk mencari nama kategori yang sama
            for (Category c:
                    categories) {
                if (c.getName().equalsIgnoreCase(nameCategory)) {
                    isExist = true;
                    break;
                }
                getIndex++;
            }
        }
        if (isExist) {
            return categories[getIndex];
        } else {
            return null;
        }

    }

    private Book getBook(String nama, String penulis){
        boolean isExist = false;
        int getIndex = 0;
        if (books != null) {
            for (Book b:
                    books) {
                if (b.getTitle().equalsIgnoreCase(nama) && b.getAuthor().equalsIgnoreCase(penulis)){
                    isExist = true;
                    break;
                }
                getIndex++;
            }
        }
        if (isExist) {
            return books[getIndex];
        } else {
            return null;
        }
    }

    private void setMembers(Member newMember){
        if (members == null) {
            members = new Member[] {newMember};
        } else {
            // Memperbesar panjang array members
            Member[] tempMembers = new Member[members.length+1];
            System.arraycopy(members, 0, tempMembers, 0, members.length);
            tempMembers[members.length] = newMember;
            members = Arrays.copyOf(tempMembers, tempMembers.length);
        }
    }

    private void setCategories(Category newCategory){
        if (categories == null) {
            categories = new Category[] {newCategory};
        } else {
            Category[] tempCategories = new Category[categories.length+1];
            System.arraycopy(categories, 0, tempCategories, 0, categories.length);
            tempCategories[categories.length] = newCategory;
            categories = Arrays.copyOf(tempCategories, tempCategories.length);
        }
    }

    private void setBooks(Book newBook){
        if (books == null) {
            books = new Book[] {newBook};
        } else {
            Book[] tempBook = new Book[books.length+1];
            System.arraycopy(books, 0, tempBook, 0, books.length);
            tempBook[books.length] = newBook;
            books = Arrays.copyOf(tempBook, tempBook.length);
        }

    }

}
