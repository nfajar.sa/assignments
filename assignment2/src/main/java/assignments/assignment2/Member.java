package assignments.assignment2;

import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Member {
    private final String id;
    private final String name;
    private final String dateOfBirth;
    private final String studyProgram;
    private final String angkatan;
    private long fine;
    private int point;
    private BookLoan[] bookLoans;

    public Member(String id, String name, String dateOfBirth, String studyProgram, String angkatan){
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.studyProgram = studyProgram;
        this.angkatan = angkatan;
    }

    @Override
    public String toString() {
        return "ID Anggota: " + this.id +
                "\nNama Anggota: " + this.name +
                "\nTotal Point: " + this.point +
                "\nDenda: " + this.fine;
    }

    private static int[] getDate(String dateFormat){
        String[] date = dateFormat.split("/");
        int[] finalDate = new int[3];
        int index = 0;
        for (String num:
                date) {
            finalDate[index++] = Integer.parseInt(num);
        }
        return finalDate;
    }

    public void pinjam(Book book, String loanDate) {
        // TODO: method untuk melakukan peminjaman oleh member
        BookLoan newBookLoan = new BookLoan(this, book, loanDate);
        if (bookLoans == null) {
            bookLoans = new BookLoan[]{newBookLoan};
        } else {
            BookLoan[] tempBookLoan = new BookLoan[bookLoans.length+1];
            System.arraycopy(bookLoans, 0, tempBookLoan, 0, bookLoans.length);
            tempBookLoan[bookLoans.length] = newBookLoan;
            this.setBookLoans(Arrays.copyOf(tempBookLoan, tempBookLoan.length));
        }
        book.setStock(book.getStock()-1);
    }

    public void kembali(Book book, String returnDate) {
        // TODO: method untuk melakukan pengembalian oleh member
        int getIndex = 0;
        for (BookLoan bookLoan:
             bookLoans) {
            if (bookLoan.getBook().equals(book)) {
                break;
            }
            getIndex++;
        }
        bookLoans[getIndex].setStatus(false);
        bookLoans[getIndex].setReturnDate(returnDate);

        // cari durasi waktu
        Calendar tempLoan = Calendar.getInstance();
        Calendar tempReturn = Calendar.getInstance();

        int[] arrLoan = getDate(bookLoans[getIndex].getLoanDate());
        int[] arrReturn = getDate(bookLoans[getIndex].getReturnDate());

        tempLoan.set(arrLoan[2], arrLoan[1], arrLoan[0]);
        tempReturn.set(arrReturn[2], arrReturn[1], arrReturn[0]);

        long compare = tempReturn.getTimeInMillis() - tempLoan.getTimeInMillis();
        long duration = TimeUnit.MILLISECONDS.toDays(compare);

        bookLoans[getIndex].setFineLoan(duration);
        book.setStock(book.getStock()+1);

        this.point += bookLoans[getIndex].getBook().getCategory().getPoint();
        System.out.println("Buku " + book.getTitle() + " berhasil dikembalikan oleh " + this.getNama() + " dengan denda Rp " + bookLoans[getIndex].getFineLoan() +"!");
    }

    public void detail() {
        // TODO: method untuk menampilkan detail anggota
        System.out.println(this);
        System.out.println("Riwayat Peminjaman Buku :");
        if(bookLoans != null){
            int pointer = 1;
            for (BookLoan bookLoan :
                 bookLoans) {
                System.out.println("----------------- " + (pointer++) + " -----------------");
                System.out.println(bookLoan);
            }
        }
    }

    public void bayarDenda(long amount) {
        // TODO: method untuk melakukan pembayaran denda
        if (this.getFine() <= amount){
            long kembalian = amount - this.getFine();
            this.setFine(0);
            System.out.println(this.getNama() + " berhasil membayar lunas denda");
            System.out.println("Jumlah kembalian: Rp " + kembalian);
        } else {
            long sisa = this.getFine() - amount;
            System.out.println(this.getNama() + " berhasil membayar denda sebesar Rp " + amount);
            this.setFine(sisa);
            System.out.println("Sisa denda saat ini: Rp " + sisa);
        }
    }

    public String getId(){
        return this.id;
    }

    public long getFine(){
        return this.fine;
    }

    public int getPoint(){
        return this.point;
    }

    public BookLoan[] getBookLoan(){
        return this.bookLoans;
    }

    public String getNama() {
        return this.name;
    }

    public void setBookLoans(BookLoan[] newBookLoans) {
        this.bookLoans = newBookLoans;
    }

    public void setFine(long newFine){
        this.fine = newFine ;
    }

    public void setPoint(int newPoint){
        this.point = newPoint;
    }
}
