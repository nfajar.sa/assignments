package assignments.assignment1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Arrays;

public class IdGenerator {
    static HashMap<Character, Integer> charToValue = new HashMap<>(36);
    static char[] valueToChar = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    static String[] dataProgramStudi = {"SIK", "SSI", "MIK", "MTI", "DIK"}; // Data program studi yang valid

    /*
     * Code pada method main tidak boleh diganti sama sekali!
     */
    public static void main(String[] args) {
        buildMapCharToValue();
        Scanner input = new Scanner(System.in);
        System.out.println("Selamat Datang di Perpustakaan!");

        boolean shouldTerminateProgram = false;

        while (!shouldTerminateProgram) {
            printMenu();
            System.out.print("Menu yang anda pilih: ");
            int menu = input.nextInt();
            input.nextLine();

            if (menu == 1) {
                System.out.print("Program Studi: ");
                String programStudi = input.nextLine();
                System.out.print("Angkatan: ");
                String angkatan = input.nextLine();
                System.out.print("Tanggal Lahir: ");
                String tanggalLahir = input.nextLine();

                System.out.println(generateId(programStudi, angkatan, tanggalLahir));
            } else if (menu == 2) {
                System.out.print("ID Anggota: ");
                String idAnggota = input.nextLine();

                System.out.print("Validitas: ");
                System.out.println(checkValidity(idAnggota));
            } else {
                shouldTerminateProgram = true;
                System.out.println("Sampai jumpa!");
            }
        }

        input.close();
    }

    /*
     * Method buildMapCodeToNumber adalah method untuk membuat mapping reference numbers Code 93
     */
    public static void buildMapCharToValue() {
        for (int count = 0; count < valueToChar.length; count++) {
            charToValue.put(valueToChar[count], count);
        }
    }

    /*
     * Method getCharFromValue adalah method yang akan mengembalikan Code 93 dari value yang diberikan
     */
    private static char getCharFromValue(int value) {
        return valueToChar[value];
    }

    /*
     * Method getValueFromChar adalah method yang akan mengembalikan value dari Code 93 yang diberikan
     */
    private static int getValueFromChar(char character) {
        return charToValue.get(character);
    }

    private static void printMenu() {
        System.out.println("--------------------------------------------");
        System.out.println("Menu yang tersedia:");
        System.out.println("1. Generate ID anggota untuk anggota baru");
        System.out.println("2. Check validitas ID anggota");
        System.out.println("3. Keluar");
        System.out.println("--------------------------------------------");
    }

    /*
     * Method generateId adalah method untuk membuat ID keanggotaan perpustakaan
     * Parameter dan return type dari method ini tidak boleh diganti
     */
    public static String generateId(String programStudi, String angkatan, String tanggalLahir){
        // TODO: Tuliskan implementasi untuk membuat ID keanggotaan perpustakaan
        String idPerpustakaan = ""; // Intial Variable
        // Jika input tidak valid
        if(!isValidInput(programStudi, angkatan, tanggalLahir)) return "Input tidak valid!";

        String[] tanggalLahirTemp = {tanggalLahir.substring(0,2), tanggalLahir.substring(3,5), tanggalLahir.substring(8)};
        // Menambahkan program studi, angkatan, dan tanggal lahir ke dalam id
        idPerpustakaan += programStudi + angkatan.substring(2) + tanggalLahirTemp[0] + tanggalLahirTemp[1] + tanggalLahirTemp[2];
        // Menambahkan checksum C ke dalam id
        idPerpustakaan += getChecksum(idPerpustakaan);  // Checksum C
        // Menambahlan checksum K ke dalam id
        idPerpustakaan += getChecksum(idPerpustakaan);  // Checksum K

        return "ID Anggota: " + idPerpustakaan;
    }

    /*
     * Method checkValidity adalah method untuk mengecek validitas ID keanggotaan perpustakaan
     * Parameter dan return type dari method ini tidak boleh diganti
     */
    public static boolean checkValidity(String idAnggota) {
        // TODO: Tuliskan implementasi untuk mengecek validitas ID keanggotaan perpustakaan
        // Jika panjang kode tidak sama dengan 13
        boolean isValidLen = idAnggota.length() == 13;
        if (!isValidLen) return false;

        String[] id = {idAnggota.substring(0,3), idAnggota.substring(3,11), idAnggota.substring(11,12), idAnggota.substring(12)};

        // Jika program studi tidak ada di dalam data program studi
        boolean isValidProgramStudi = Arrays.asList(dataProgramStudi).contains(id[0]);
        if (!isValidProgramStudi) return false;

        // Jika tahun angkatan tidak sesuai format dan di luar range 2000 - 2021
        String angkatan = id[1].substring(0,2);
        boolean isValidAngkatan = (Integer.parseInt("20"+angkatan) >= 2000) && (Integer.parseInt("20"+angkatan) <= 2021);
        if (!isValidAngkatan) return  false;

        // Jika karakter checksum tidak uppercase, ada karakter numerik coy asw
        boolean isValidChecksum = (Character.isUpperCase(id[2].charAt(0)) || Character.isDigit(id[2].charAt(0)))
                                  && Character.isUpperCase(id[3].charAt(0)) || Character.isDigit(id[3].charAt(0));
        if (!isValidChecksum) return false;

        // Jika checksum C dan checksum K tidak sesuai
        char checksumC = getChecksum(id[0]+id[1]);
        char checksumK = getChecksum(id[0]+id[1]+id[2]);
        return (Character.toString(checksumC).equals(id[2])) && (Character.toString(checksumK).equals(id[3]));
    }

    /*
     * Method isValidInput adalah method untuk mengecek validitas input ID keanggotaan perpustakaan
     * @return boolean validitas input
     */
    public static boolean isValidInput(String programStudi, String angkatan, String tanggalLahir) {
        try{
            int angkatanTemp = Integer.parseInt(angkatan);
            boolean isValidAngkatan = (angkatanTemp >= 2000) && (angkatanTemp <= 2021);
            boolean isValidProgramStudi = Arrays.asList(dataProgramStudi).contains(programStudi);
            if (!isValidAngkatan) return false;     // Validasi input angkatan
            if (!isValidProgramStudi) return false;     // Validasi input program studi
            if (!isValidDate(tanggalLahir)) return false;   // Validasi input tanggal lahir
            return true;
        } catch (NumberFormatException e){  // Handling NumberFormatException, ketika input angkatan bukan numerik
            return false;
        }
    }

    /*
     * Method isValidDate adalah method untuk mengecek validitas tanggal
     * @return boolean validitas tanggal
     */
    public static boolean isValidDate(String tanggal) {
        String[] splitTanggal = tanggal.split("/");
        try {
            int hari = Integer.parseInt(splitTanggal[0]);
            int bulan = Integer.parseInt(splitTanggal[1]);
            int tahun = Integer.parseInt(splitTanggal[2]);

            boolean isValidHari = 1 <= hari && hari <=31;   // Generalisasi tiap bulan 31 hari
            boolean isValidBulan = 1 <= bulan && bulan <= 12;
            boolean isValidTahun = 1945 <= tahun && tahun <= 2021;  // Range hanya asumsi, ini untuk validasi input terakhir setelah '/' merupakan tahun
            if (!(isValidHari && isValidBulan && isValidTahun)) return false;   // Validasi tanggal

            return (splitTanggal[0].length() == 2) && (splitTanggal[1].length() == 2) && (splitTanggal[2].length() == 4);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {    // Handling NumberFormatException, ketika input tanggal bukan numerik
            return false;                                                       // Handling ArrayIndexOutOfBoundsException, ketika format input tanggal tidak sesuai
        }
    }

    /*
     * Method getChecksum adalah method untuk menghitung checksum C dan K tergantung panjang karakter stringnya
     * @return char checksum
     */
    public static char getChecksum(String karakter){
        int checksum = 0;
        ArrayList<Integer> values = new ArrayList<>();      // bisa langsung tambah
        int len = karakter.length();
        int i = 0;

        while (len != 0){       // Iterasi setiap karakter dalam string
            int value = getValueFromChar(karakter.charAt(i));
            values.add(value*len);  // dikali bobot karakter dan dimasukkan ke dalam arraylist
            len--;
            i++;
        }

        for (int Datum:values) {    // Penjumlahan total
            checksum += Datum;
        }

        return getCharFromValue(checksum%36);
    }

}
